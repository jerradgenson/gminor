"""
Copyright 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import os
from functools import partial

import antlr4
from antlr4.error.ErrorListener import ErrorListener

import gminor
import preprocessor
from integration import testcase
from syntax_analysis.failfast_lexer import FailfastLexer as GminorLexer
from syntax_analysis.gminor_parser import gminorParser as GminorParser
from syntax_analysis.failfast_lexer import TokenRecognitionError
from syntax_analysis.base_listener import gminorListener as GminorListener
from semantic_analysis import symbols

gminortest = partial(testcase, '.gmi')


@gminortest('.tokens', name='lexer')
def test_lexer(test_program):
    input_stream = antlr4.FileStream(test_program)
    lexer = GminorLexer(input_stream)
    token_string = ''
    token = lexer.nextToken()
    while token.type != token.EOF:
        token_type = '<' + lexer.symbolicNames[token.type] + '>'
        token_string += token_type + ' ' + token.text + '\n'
        try:
            token = lexer.nextToken()

        except TokenRecognitionError as tre:
            token_string += str(tre) + '\n'
            return token_string

    token_string += '<EOF>\n'
    return token_string


@gminortest('.ast', name='parser')
def test_parser(test_program):
    input_stream = antlr4.FileStream(test_program)
    lexer = GminorLexer(input_stream)
    token_stream = antlr4.CommonTokenStream(lexer)
    parser = GminorParser(token_stream)
    error_listener = TestErrorListener()
    parser.addErrorListener(error_listener)
    try:
        parse_tree = parser.program()

    except ValueError as value_error:
        return str(value_error)

    walker = antlr4.ParseTreeWalker()
    listener = TestListener()
    walker.walk(listener, parse_tree)

    return listener.ast_string


@gminortest('.preout', name='preprocessor')
def test_preprocessor(test_program):
    symbols.gen_label = symbols._create_gen_label()
    input_stream = antlr4.FileStream(test_program)
    lexer = GminorLexer(input_stream)
    token_stream = antlr4.CommonTokenStream(lexer)
    parser = GminorParser(token_stream)
    parser.removeErrorListeners()
    parser.addErrorListener(gminor.GminorErrorListener)
    parse_tree = parser.program()
    program_name = os.path.splitext(os.path.basename(test_program))[0]
    working_directory = os.path.dirname(test_program)
    pprocessor = preprocessor.Preprocessor(program_name, working_directory)
    preprocessor_output = pprocessor.visit(parse_tree)
    preprocessor_output += '\nMacro symbols\n'
    macro_symbols = ''
    for count, namespace in enumerate(pprocessor.symbol_table._stack):
        macro_symbols += '\n'.join(namespace.symbols)
        if count == len(pprocessor.symbol_table._stack) - 1 and macro_symbols:
            macro_symbols += '\n'

    preprocessor_output += macro_symbols
    preprocessor_output += '\nImported symbols\n'
    imported_symbols = '\n'.join(symbol.id for symbol in pprocessor.imported_symbols)
    preprocessor_output += imported_symbols
    preprocessor_output += '\n' if imported_symbols else ''

    return preprocessor_output


class TestListener(GminorListener):

    def __init__(self, *args, **kwargs):
        self.ast_string = ''
        super().__init__(*args, **kwargs)

    # Enter a parse tree produced by gminorParser#integer_literal.
    def enterInteger_literal(self, ctx:GminorParser.Integer_literalContext):
        self.ast_string += '<integer_literal>\n'

    # Enter a parse tree produced by gminorParser#float_literal.
    def enterFloat_literal(self, ctx:GminorParser.Float_literalContext):
        self.ast_string += '<float_literal>\n'

    # Enter a parse tree produced by gminorParser#boolean_literal.
    def enterBoolean_literal(self, ctx:GminorParser.Boolean_literalContext):
        self.ast_string += '<boolean_literal>\n'

    # Enter a parse tree produced by gminorParser#string_literal.
    def enterString_literal(self, ctx:GminorParser.String_literalContext):
        self.ast_string += '<string_literal>\n'

    # Enter a parse tree produced by gminorParser#user_identifier.
    def enterUser_identifier(self, ctx:GminorParser.User_identifierContext):
        self.ast_string += '<user_identifier>\n'

    # Enter a parse tree produced by gminorParser#identifier.
    def enterIdentifier(self, ctx:GminorParser.IdentifierContext):
        self.ast_string += '<identifier>\n'

    # Enter a parse tree produced by gminorParser#atom.
    def enterAtom(self, ctx:GminorParser.AtomContext):
        self.ast_string += '<atom>\n'

    # Enter a parse tree produced by gminorParser#procedure_call.
    def enterProcedure_call(self, ctx:GminorParser.Procedure_callContext):
        self.ast_string += '<procedure_call>\n'

    # Enter a parse tree produced by gminorParser#s_expression.
    def enterS_expression(self, ctx:GminorParser.S_expressionContext):
        self.ast_string += '<s_expression>\n'

    # Enter a parse tree produced by gminorParser#variable_binding.
    def enterVariable_binding(self, ctx:GminorParser.Variable_bindingContext):
        self.ast_string += '<variable_binding>\n'

    # Enter a parse tree produced by gminorParser#type_definition.
    def enterType_definition(self, ctx:GminorParser.Type_definitionContext):
        self.ast_string += '<type_definition>\n'

    # Enter a parse tree produced by gminorParser#parameter_list.
    def enterParameter_list(self, ctx:GminorParser.Parameter_listContext):
        self.ast_string += '<parameter_list>\n'

    # Enter a parse tree produced by gminorParser#procedure.
    def enterProcedure(self, ctx:GminorParser.ProcedureContext):
        self.ast_string += '<procedure>\n'

    # Enter a parse tree produced by gminorParser#else_clause.
    def enterElse_clause(self, ctx:GminorParser.Else_clauseContext):
        self.ast_string += '<else_clause>\n'

    # Enter a parse tree produced by gminorParser#cond_expression.
    def enterCond_expression(self, ctx:GminorParser.Cond_expressionContext):
        self.ast_string += '<cond_expression>\n'

    # Enter a parse tree produced by GminorParser#cond_subexpression.
    def enterCond_subexpression(self, ctx:GminorParser.Cond_subexpressionContext):
        self.ast_string += '<cond_subexpression>\n'

    # Enter a parse tree produced by gminorParser#let_binding.
    def enterLet_binding(self, ctx:GminorParser.Let_bindingContext):
        self.ast_string += '<let_binding>\n'

    # Enter a parse tree produced by gminorParser#let_expression.
    def enterLet_expression(self, ctx:GminorParser.Let_expressionContext):
        self.ast_string += '<let_expression>\n'

    # Enter a parse tree produced by gminorParser#macro_template.
    def enterMacro_template(self, ctx:GminorParser.Macro_templateContext):
        self.ast_string += '<macro_template>\n'

    # Enter a parse tree produced by gminorParser#syntax_rule.
    def enterSyntax_rule(self, ctx:GminorParser.Syntax_ruleContext):
        self.ast_string += '<syntax_rule>\n'

    # Enter a parse tree produced by gminorParser#keyword_list.
    def enterKeyword_list(self, ctx:GminorParser.Keyword_listContext):
        self.ast_string += '<keyword_list>\n'

    # Enter a parse tree produced by gminorParser#macro_expression.
    def enterMacro_expression(self, ctx:GminorParser.Macro_expressionContext):
        self.ast_string += '<macro_expression>\n'

    # Enter a parse tree produced by gminorParser#module_accessor.
    def enterModule_accessor(self, ctx:GminorParser.Module_accessorContext):
        self.ast_string += '<module_accessor>\n'

    # Enter a parse tree produced by gminorParser#module_import.
    def enterModule_import(self, ctx:GminorParser.Module_importContext):
        self.ast_string += '<module_import>\n'

    # Enter a parse tree produced by gminorParser#import_from.
    def enterImport_from(self, ctx:GminorParser.Import_fromContext):
        self.ast_string += '<import_from>\n'

    # Enter a parse tree produced by gminorParser#program.
    def enterProgram(self, ctx:GminorParser.ProgramContext):
        self.ast_string += '<program>\n'


class TestErrorListener(ErrorListener):
    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        raise ValueError('line {}:{} syntax error - {}\n'.format(line, column, msg))
