"""
Contains standard library definitions included in the Gminor toplevel.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

from semantic_analysis import gminor_types
from semantic_analysis.symbols import StackFrame, SymbolDescriptor, gen_label


def create_global_symbols():
    """
    Returns an instance of symbols.Namespace with all the definitions that
    should be included in the Gminor toplevel namespace.

    """

    global_symbols = dict()
    stack_frame = StackFrame('__main__', None, None, global_symbols)
    int_operator = gminor_types.ProcedureType(gminor_types.IntegerType(),
                                            operand1=gminor_types.IntegerType(),
                                            **{'.': gminor_types.IntegerType()})

    float_operator = gminor_types.ProcedureType(gminor_types.FloatType(),
                                              operand1=gminor_types.FloatType(),
                                              **{'.': gminor_types.FloatType()})

    boolean_operator = gminor_types.ProcedureType(gminor_types.BooleanType(),
                                                operand1=gminor_types.BooleanType(),
                                                **{'.': gminor_types.BooleanType()})

    unary_boolean_operator = gminor_types.ProcedureType(gminor_types.BooleanType(),
                                                      operand=gminor_types.BooleanType())

    global_symbols['+'] = SymbolDescriptor('+', int_operator, gen_label())
    global_symbols['+.'] = SymbolDescriptor('+.', float_operator, gen_label())
    global_symbols['-'] = SymbolDescriptor('-', int_operator, gen_label())
    global_symbols['-.'] = SymbolDescriptor('-.', float_operator, gen_label())
    global_symbols['*'] = SymbolDescriptor('*', int_operator, gen_label())
    global_symbols['*.'] = SymbolDescriptor('*.', float_operator, gen_label())
    global_symbols['/'] = SymbolDescriptor('/', int_operator, gen_label())
    global_symbols['/.'] = SymbolDescriptor('/.', float_operator, gen_label())
    global_symbols['='] = SymbolDescriptor('=', int_operator, gen_label())
    global_symbols['=.'] = SymbolDescriptor('=.', float_operator, gen_label())
    global_symbols['>'] = SymbolDescriptor('>', int_operator, gen_label())
    global_symbols['>.'] = SymbolDescriptor('>.', float_operator, gen_label())
    global_symbols['>='] = SymbolDescriptor('>=', int_operator, gen_label())
    global_symbols['>=.'] = SymbolDescriptor('>=.', float_operator, gen_label())
    global_symbols['<'] = SymbolDescriptor('<', int_operator, gen_label())
    global_symbols['<.'] = SymbolDescriptor('<.', float_operator, gen_label())
    global_symbols['<='] = SymbolDescriptor('<=', int_operator, gen_label())
    global_symbols['<=.'] = SymbolDescriptor('<=.', float_operator, gen_label())
    global_symbols['^'] = SymbolDescriptor('^', float_operator, gen_label())
    global_symbols['mod'] = SymbolDescriptor('mod', int_operator, gen_label())
    global_symbols['not'] = SymbolDescriptor('not', unary_boolean_operator, gen_label())
    global_symbols['or'] = SymbolDescriptor('or', boolean_operator, gen_label())
    global_symbols['and'] = SymbolDescriptor('and', boolean_operator, gen_label())

    list_type = gminor_types.PolymorphicType()
    global_symbols['list'] = SymbolDescriptor('list',
                                              gminor_types.ProcedureType(gminor_types.ListType(list_type),
                                                                       element1=list_type,
                                                                       **{'.': list_type}),
                                              gen_label())

    head_type = gminor_types.PolymorphicType()
    global_symbols['head'] = SymbolDescriptor('head',
                                              gminor_types.ProcedureType(head_type,
                                                                       tlist=gminor_types.ListType(head_type)),
                                              gen_label())

    tail_type = gminor_types.ListType(gminor_types.PolymorphicType())
    global_symbols['tail'] = SymbolDescriptor('tail',
                                              gminor_types.ProcedureType(tail_type, tlist=tail_type),
                                              gen_label())

    pair_element_type = gminor_types.PolymorphicType()
    pair_list_type = gminor_types.ListType(pair_element_type)
    global_symbols['prepend'] = SymbolDescriptor('prepend',
                                                 gminor_types.ProcedureType(pair_list_type,
                                                                          element=pair_element_type,
                                                                          tlist=pair_list_type),
                                                 gen_label())

    append_element_type = gminor_types.PolymorphicType()
    append_list_type = gminor_types.ListType(append_element_type)
    global_symbols['append'] = SymbolDescriptor('append',
                                                gminor_types.ProcedureType(append_list_type,
                                                                         tlist=append_list_type,
                                                                         element=append_element_type),
                                                gen_label())

    global_symbols['print'] = SymbolDescriptor('print',
                                               gminor_types.ProcedureType(gminor_types.ListType(gminor_types.PolymorphicType),
                                                                        string=gminor_types.StringType()),
                                               gen_label())

    length_type = gminor_types.IntegerType()
    global_symbols['length'] = SymbolDescriptor('length',
                                                gminor_types.ProcedureType(length_type,
                                                                         tlist=gminor_types.PolymorphicType()),
                                                gen_label())

    index_type = gminor_types.PolymorphicType()
    global_symbols['index'] = SymbolDescriptor('index',
                                               gminor_types.ProcedureType(index_type,
                                                                        tlist=gminor_types.ListType(index_type)),
                                               gen_label())

    extend_type = gminor_types.ListType(gminor_types.PolymorphicType())
    global_symbols['extend'] = SymbolDescriptor('extend',
                                                gminor_types.ProcedureType(extend_type,
                                                                         tlist1=extend_type,
                                                                         tlist2=extend_type,
                                                                         _remainder=extend_type),
                                                gen_label())

    filter_list_element_type = gminor_types.PolymorphicType()
    filter_list_type = gminor_types.ListType(filter_list_element_type)
    filter_procedure_type = gminor_types.ProcedureType(filter_list_type,
                                                     procedure=gminor_types.ProcedureType(gminor_types.BooleanType(),
                                                                                        x=filter_list_element_type))

    global_symbols['filter'] = SymbolDescriptor('filter',
                                                filter_procedure_type,
                                                gen_label())

    global_symbols['str-append'] = SymbolDescriptor('str-append',
                                                    gminor_types.ProcedureType(gminor_types.StringType(),
                                                                             str1=gminor_types.StringType(),
                                                                             str2=gminor_types.StringType(),
                                                                             _remainder=gminor_types.StringType()),
                                                    gen_label())

    return stack_frame
