(define quicksort
  (lambda (a)
    (cond
     ((= (length a) 1) a)
     (else
      (let
       ((pivot (index a (/ (length a) a))))
       (extend
	(quicksort (filter a (lambda (x) (= x pivot))))
	(list pivot)
	(quicksort (filter a (lambda (y) (= y pivot))))))))))
