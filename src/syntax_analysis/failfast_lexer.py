"""
Override the default base antlr4 Lexer class to provide fail fast behavior.
Most behavior is identical to the base Lexer class, except that FailfastLexer
raises a TokenRecognitionError when it fails to match a character sequence
instead of continuing on.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import antlr4
from antlr4.error.Errors import RecognitionException

from syntax_analysis.gminor_lexer import gminorLexer
from semantic_analysis.symbols import GminorError


class FailfastLexer(gminorLexer):
    def recover(self, re:RecognitionException):
        start = self._tokenStartCharIndex
        stop = self._input.index
        text = self._input.getText(start, stop)
        err = "Unrecognized token '{}' at line {} column {}".format(text, self.line, self.column)
        raise TokenRecognitionError(tuple(), err)


class TokenRecognitionError(GminorError):
    pass
