"""
Unit tests for semantic_analysis/preprocessor.py.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import unittest
from unittest.mock import Mock, MagicMock, PropertyMock, patch

from pyparsing import OneOrMore, Word, StringEnd, empty, nums, alphas

import preprocessor
from syntax_analysis import macro_grammar
from semantic_analysis import symbols


class TestReconstruct(unittest.TestCase):
    def test_empty_list(self):
        original = '()'
        parser = empty + '(' + ')'
        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_integer_literal(self):
        original = '42'
        parser = empty + Word(original)
        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_float_literal(self):
        original = '3.14'
        parser = empty + Word(original)
        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_string_literal(self):
        original = 'My heart will go on.'
        parser = empty + Word(original)
        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_boolean_literal(self):
        original = '#t'
        parser = empty + Word(original)
        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_identifier(self):
        original = 'some-variable'
        parser = empty + macro_grammar.s_expression().setResultsName(original, True)
        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_procedure_call(self):
        original = '(procedure arg1 arg2)'
        parser = empty + '(' + Word('procedure') + Word('arg1') + Word('arg2') + ')'
        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_procedure(self):
        original = '(lambda (arg1 arg2) (+ arg1 arg2))'
        parser = (empty
                  + '('
                  + Word('lambda')
                  + '('
                  + Word('arg1')
                  + Word('arg2')
                  + ')'
                  + '('
                  + '+'
                  + Word('arg1')
                  + Word('arg2')
                  + ')'
                  + ')')

        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_cond_expression(self):
        original = '(cond (#f "true") (else "false"))'
        parser = (empty
                  + '('
                  + Word('cond')
                  + '('
                  + Word('#f')
                  + Word('"true"')
                  + ')'
                  + '('
                  + Word('else')
                  + Word('"false"')
                  + ')'
                  + ')')

        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_let_expression(self):
        original = '(let ((a 1) (b 2)) (+ a b))'
        parser = (empty
                  + '('
                  + Word('let')
                  + '('
                  + '('
                  + Word('a')
                  + Word('1')
                  + ')'
                  + '('
                  + Word('b')
                  + Word('2')
                  + ')'
                  + ')'
                  + '('
                  + Word('+')
                  + Word('a')
                  + Word('b')
                  + ')'
                  + ')')

        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)

    def test_compound_list(self):
        original = '(define-type new-type list poly new-type) (define nt new-type)'
        parser = (empty
                  + '('
                  + Word('define-type')
                  + Word('new-type')
                  + Word('list')
                  + Word('poly')
                  + Word('new-type')
                  + ')'
                  + '('
                  + Word('define')
                  + Word('nt')
                  + Word('new-type')
                  + ')')

        s_expr = parser.parseString(original)
        reconstructed = preprocessor.reconstruct(s_expr)
        self.assertEqual(reconstructed, original)


class TestSemanticElement(unittest.TestCase):
    def test_create_text_element(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__, text='test text')
        self.assertEqual(semantic_element.text, 'test text')

    def test_create_parser_element(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        parser_element='test parser element')

        self.assertEqual(semantic_element.parser_element, 'test parser element')

    def test_create_symbol_descriptor(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        symbol_descriptor='test symbol desc')

        self.assertEqual(semantic_element.symbol_descriptor, 'test symbol desc')

    def test_raise_value_error1(self):
        self.assertRaises(ValueError,
                          preprocessor.SemanticElement,
                          type(self).__name__,
                          text='test text',
                          parser_element='test parser element')

    def test_raise_value_error2(self):
        self.assertRaises(ValueError,
                          preprocessor.SemanticElement,
                          type(self).__name__,
                          text='test text',
                          symbol_descriptor='test symbol descriptor')

    def test_raise_value_error3(self):
        self.assertRaises(ValueError,
                          preprocessor.SemanticElement,
                          type(self).__name__,
                          parser_element='test parser element',
                          symbol_descriptor='test symbol descriptor')

    def test_raise_value_error4(self):
        self.assertRaises(ValueError,
                          preprocessor.SemanticElement,
                          type(self).__name__,
                          text='test text',
                          parser_element='test parser element',
                          symbol_descriptor='test symbol descriptor')

    def test_raise_value_error5(self):
        self.assertRaises(ValueError,
                          preprocessor.SemanticElement,
                          type(self).__name__)

    def test_access_text_attribute(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        text='test text')

        self.assertEqual(getattr(semantic_element, 'text'), 'test text')

    def test_access_parser_element_attribute(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        parser_element='parser element')

        self.assertEqual(getattr(semantic_element, 'parser_element'), 'parser element')

    def test_access_symbol_descriptor_attribute(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        symbol_descriptor='test symbol desc')

        self.assertEqual(getattr(semantic_element, 'symbol_descriptor'), 'test symbol desc')

    def test_raise_attribute_error1(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        text='test text')

        self.assertRaises(AttributeError,
                          getattr,
                          semantic_element,
                          'parser_element')

    def test_raise_attribute_error2(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        parser_element='test parser element')

        self.assertRaises(AttributeError,
                          getattr,
                          semantic_element,
                          'symbol_descriptor')

    def test_raise_attribute_error3(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__,
                                                        symbol_descriptor='test symbol desc')

        self.assertRaises(AttributeError,
                          getattr,
                          semantic_element,
                          'text')

    def test_str(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__, text='test text')
        self.assertEqual(str(semantic_element), 'SemanticElement(TestSemanticElement)')

    def test_access_undefined_attribute(self):
        semantic_element = preprocessor.SemanticElement(type(self).__name__, text='test text')
        self.assertRaises(AttributeError,
                          getattr,
                          semantic_element,
                          'not_defined')


class TestFindModule(unittest.TestCase):
    def test_module_not_found_no_gminor_path(self):
        with patch('os.environ', autospec=True) as environ, patch('os.listdir', autospec=True) as listdir:
            environ.get.return_value = None
            listdir.return_value = []
            source_path = preprocessor.find_module('testfile', 'testdir')

        self.assertEqual(source_path, '')

    def test_module_not_found_one_gminor_path(self):
        with patch('os.environ', autospec=True) as environ, patch('os.listdir', autospec=True) as listdir:
            environ.get.return_value = 'gminor_packages'
            listdir.return_value = ['randofile1', 'randofile2', 'randofile3']
            source_path = preprocessor.find_module('testfile', 'testdir')

        self.assertEqual(len(listdir.mock_calls), 2)
        self.assertEqual(listdir.mock_calls[1][1][0], 'gminor_packages')
        self.assertEqual(source_path, '')

    def test_module_not_found_two_gminor_paths(self):
        with patch('os.environ', autospec=True) as environ, patch('os.listdir', autospec=True) as listdir:
            environ.get.return_value = 'gminor_packages1:gminor_packages2'
            listdir.return_value = ['randofile1', 'randofile2', 'randofile3']
            source_path = preprocessor.find_module('testfile', 'testdir')

        self.assertEqual(len(listdir.mock_calls), 3)
        self.assertEqual(listdir.mock_calls[1][1][0], 'gminor_packages1')
        self.assertEqual(listdir.mock_calls[2][1][0], 'gminor_packages2')
        self.assertEqual(source_path, '')

    def test_module_in_working_directory(self):
        with patch('os.environ', autospec=True) as environ, patch('os.listdir', autospec=True) as listdir:
            environ.get.return_value = None
            listdir.return_value = ['randofile1', 'randofile2', 'testfile.sym']
            source_path = preprocessor.find_module('testfile', 'testdir')

        self.assertEqual(len(listdir.mock_calls), 1)
        self.assertEqual(listdir.mock_calls[0][1][0], 'testdir')
        self.assertEqual(source_path, 'testdir/testfile.sym')

    def test_module_in_gminor_path(self):
        def create_listdir():
            def listdir_mock(directory):
                if directory == 'gminor_packages2':
                    return ['randofile1', 'randofile2', 'testfile.sym']

                return []

            return listdir_mock

        with patch('os.environ', autospec=True) as environ,\
             patch('os.listdir', new_callable=create_listdir):
            environ.get.return_value = 'gminor_packages1:gminor_packages2'
            source_path = preprocessor.find_module('testfile', 'testdir')

        self.assertEqual(source_path, 'gminor_packages2/testfile.sym')


class TestUnboxSemanticElement(unittest.TestCase):
    def test_raise_value_error(self):
        semantic_element = MagicMock()
        self.assertRaises(ValueError, preprocessor.unbox_semantic_element, semantic_element, 0, 0, 0)

    def test_raise_attribute_error_no_text_element(self):
        class SemanticElement:
            @property
            def symbol_descriptor(self):
                return 'success'

        semantic_element = SemanticElement()
        self.assertRaises(AttributeError, preprocessor.unbox_semantic_element, semantic_element, 1, 0, 0)

    def test_raise_attribute_error_no_parser_element(self):
        class SemanticElement:
            @property
            def text(self):
                return 'success'

        semantic_element = SemanticElement()
        self.assertRaises(AttributeError, preprocessor.unbox_semantic_element, semantic_element, 0, 1, 0)

    def test_raise_attribute_error_no_symbol_descriptor(self):
        class SemanticElement:
            @property
            def parser_element(self):
                return 'success'

        semantic_element = SemanticElement()
        self.assertRaises(AttributeError, preprocessor.unbox_semantic_element, semantic_element, 0, 0, 1)

    def test_text_only(self):
        semantic_element = MagicMock()
        text = PropertyMock()
        text.return_value = 'success'
        type(semantic_element).text = text
        return_value = preprocessor.unbox_semantic_element(semantic_element, 1, 0, 0)
        self.assertEqual(return_value, 'success')
        self.assertEqual(len(text.mock_calls), 1)
        self.assertEqual(len(semantic_element.mock_calls), 0)

    def test_parser_element_only(self):
        semantic_element = MagicMock()
        parser_element = PropertyMock()
        parser_element.return_value = 'success'
        type(semantic_element).parser_element = parser_element
        return_value = preprocessor.unbox_semantic_element(semantic_element, 0, 1, 0)
        self.assertEqual(return_value, 'success')
        self.assertEqual(len(parser_element.mock_calls), 1)
        self.assertEqual(len(semantic_element.mock_calls), 0)

    def test_symbol_descriptor_only(self):
        semantic_element = MagicMock()
        symbol_descriptor = PropertyMock()
        symbol_descriptor.return_value = 'success'
        type(semantic_element).symbol_descriptor = symbol_descriptor
        return_value = preprocessor.unbox_semantic_element(semantic_element, 0, 0, 1)
        self.assertEqual(return_value, 'success')
        self.assertEqual(len(symbol_descriptor.mock_calls), 1)
        self.assertEqual(len(semantic_element.mock_calls), 0)

    def test_text_and_parser_element(self):
        semantic_element = Mock(spec=['parser_element'])
        parser_element = PropertyMock()
        parser_element.return_value = 'success'
        type(semantic_element).parser_element = parser_element
        return_value = preprocessor.unbox_semantic_element(semantic_element, 1, 2, 0)
        self.assertEqual(return_value, 'success')
        self.assertEqual(len(parser_element.mock_calls), 1)

    def test_text_and_parser_element_and_symbol_descriptor(self):
        semantic_element = Mock(spec=['symbol_descriptor'])
        symbol_descriptor = PropertyMock()
        symbol_descriptor.return_value = 'success'
        type(semantic_element).symbol_descriptor = symbol_descriptor
        return_value = preprocessor.unbox_semantic_element(semantic_element, 2, 3, 1)
        self.assertEqual(return_value, 'success')
        self.assertEqual(len(symbol_descriptor.mock_calls), 1)


class TestApplyMacro(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        pattern1 = OneOrMore(Word(nums)).setResultsName('digits', True) + StringEnd()
        template1 = '--Digits: {digits}--'
        pattern2 = OneOrMore(Word(alphas)).setResultsName('letters', True) + StringEnd()
        template2 = '--Letters: {letters}--'
        pattern3 = (OneOrMore(Word(alphas)).setResultsName('letters', True)
                    + OneOrMore(Word(nums)).setResultsName('digits', True)
                    + StringEnd())

        template3 = '{digits}{letters}'
        self.syntax_rules = [(pattern1, template1),
                             (pattern2, template2),
                             (pattern3, template3)]

        super().__init__(*args, **kwargs)

    def test_empty_syntax_rules(self):
        s_expressions = '+ 1 2'
        self.assertRaises(symbols.GminorSyntaxError,
                          preprocessor.apply_macro,
                          tuple(),
                          s_expressions)

    def test_empty_s_expressions(self):
        self.assertRaises(symbols.GminorSyntaxError,
                          preprocessor.apply_macro,
                          self.syntax_rules,
                          '')

    def test_no_matching_pattern(self):
        s_expressions = '+ 1 2'
        self.assertRaises(symbols.GminorSyntaxError,
                          preprocessor.apply_macro,
                          self.syntax_rules,
                          s_expressions)

    def test_match_nums_pattern(self):
        s_expressions = '123456789'
        semantic_element = preprocessor.apply_macro(self.syntax_rules, s_expressions)
        self.assertEqual(semantic_element.text, '--Digits: 123456789--')

    def test_match_alphas_pattern(self):
        s_expressions = 'qwertyuiop'
        semantic_element = preprocessor.apply_macro(self.syntax_rules, s_expressions)
        self.assertEqual(semantic_element.text, '--Letters: qwertyuiop--')

    def test_match_alphanumeric_pattern(self):
        s_expressions = 'qwertyuiop123456789'
        semantic_element = preprocessor.apply_macro(self.syntax_rules, s_expressions)
        self.assertEqual(semantic_element.text, '123456789qwertyuiop')
