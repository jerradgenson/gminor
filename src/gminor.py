#! /usr/bin/python3
"""
Contains the Gminor controller and command line interface.
This is the main module when Gminor is invoked from the command line.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import os
import sys
import argparse
import logging
import pickle
from pathlib import Path

import antlr4

import preprocessor
from lib import toplevel
from syntax_analysis.failfast_lexer import FailfastLexer as GminorLexer
from syntax_analysis.gminor_parser import gminorParser as GminorParser
from semantic_analysis import semantic_analyzer
from semantic_analysis import symbols


__program__ = 'Gminor'
__version__ = 0.1


def main():
    """
    Gminor's "main" function. Execution starts here when invoked from the
    command line.

    """

    clargs = parse_command_line()
    configure_logging(clargs.log_level)
    try:
        if clargs.input_path:
            macro_symbols, regular_symbols, mangled_names, compiled_code =\
                compile_file(clargs.input_path)

            symbol_file = clargs.output.with_suffix(clargs.output.suffix + '.sym')
            with symbol_file.open('wb') as output_file:
                pickle.dump((macro_symbols, regular_symbols, mangled_names), output_file)

        else:
            print('{} version {}\n'.format(__program__, __version__))
            global_symbols = toplevel.create_global_symbols()
            symbol_table = symbols.SymbolTable(global_symbols)
            while True:
                code_string = input('# ')
                symbol_table = compile_string(code_string, symbol_table)

    except symbols.GminorError as gminor_error:
        report_error(gminor_error)
        return 1

    except KeyboardInterrupt:
        pass

    return 0


def parse_command_line():
    """
    Parse arguments from the command line and return the parsed arguments.

    """

    parser = argparse.ArgumentParser(description='Compile a Gminor program.')
    parser.add_argument('input_path',
                        type=Path,
                        help='Path to a Gminor source file.')

    parser.add_argument('-o', '--output',
                        default='a.out',
                        type=Path,
                        help='File to write compiled code to.')

    parser.add_argument('-d', '--debug',
                        action='store_const',
                        const=logging.DEBUG,
                        default=logging.INFO,
                        dest='log_level',
                        help='Run Gminor in debug mode.')

    clargs = parser.parse_args()

    return clargs


def configure_logging(log_level):
    """
    Configure root logger with the given log level.

    """

    root_logger = logging.getLogger()
    root_logger.setLevel(log_level)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(log_level)
    formatter = logging.Formatter('%(message)s')
    console_handler.setFormatter(formatter)
    root_logger.addHandler(console_handler)


def report_error(exception):
    """
    Log a GminorError exception with loglevel ERROR.

    """

    logger = logging.getLogger(__name__)
    error_message = ''
    if exception.stack_trace:
        error_message += 'Traceback (most recent call last):'
        error_message += '\n  '
        error_message += '\n  '.join(exception.stack_trace)

    error_message += '\n{}: {}'.format(exception, exception.info)
    logger.error(error_message)


def compile_file(path):
    """
    Compile the Gminor source file at the target path and return its namespace.

    """

    input_stream = antlr4.FileStream(str(path))
    global_symbols = toplevel.create_global_symbols()
    symbol_table = symbols.SymbolTable(global_symbols)
    working_directory = os.path.abspath(os.path.dirname(path))
    program_name = os.path.splitext(os.path.basename(path))[0]
    return _compile_program(input_stream,
                            program_name,
                            symbol_table,
                            working_directory)


def compile_string(code_string, symbol_table):
    """
    Compile the target Gminor source code string and return its namespace.

    """

    return _compile_program(antlr4.InputStream(code_string),
                            'code string',
                            symbol_table,
                            os.getcwd())


def _compile_program(input_stream, program_name, symbol_table, working_directory):
    logger = logging.getLogger(__name__)

    # Preprocessor
    logger.debug('Compilation stage: syntax analysis (pass 1)')
    preprocess_lexer = GminorLexer(input_stream)
    preprocess_token_stream = antlr4.CommonTokenStream(preprocess_lexer)
    preprocess_parser = GminorParser(preprocess_token_stream)
    preprocess_parser.removeErrorListeners()
    preprocess_parser.addErrorListener(GminorErrorListener)
    preprocess_parser_tree = preprocess_parser.program()
    logger.debug('Compilation stage: preprocessor')
    pprocessor = preprocessor.Preprocessor(program_name, working_directory)
    preprocessor_output = pprocessor.visit(preprocess_parser_tree)

    # Add symbols from imported modules to the current module's symbol table.
    for symbol in pprocessor.imported_symbols:
        symbol_table.add_symbol(symbol)

    # Main compiler
    logger.debug('Compilation stage: syntax analysis (pass 2)')
    postprocess_lexer = GminorLexer(antlr4.InputStream(preprocessor_output))
    postprocess_token_stream = antlr4.CommonTokenStream(postprocess_lexer)
    gminor_parser = GminorParser(postprocess_token_stream)
    gminor_parser.removeErrorListeners()
    gminor_parser.addErrorListener(GminorErrorListener)
    postprocess_parse_tree = gminor_parser.program()
    logger.debug('Compilation stage: semantic analysis')
    analyzer = semantic_analyzer.SemanticAnalyzer(program_name, symbol_table)
    analyzer.visit(postprocess_parse_tree)

    return pprocessor.symbol_table, analyzer.symbol_table, pprocessor.mangled_names, ''


class GminorErrorListener(antlr4.error.ErrorListener.ErrorListener):
    def syntaxError(self, offending_symbol, line, column, msg, error):
        info = 'line {} column {} - {}'.format(line, column, msg)
        raise symbols.GminorSyntaxError(tuple(), info)


if __name__ == '__main__':
    sys.exit(main())
