"""
Unit and integration tests for Gminor.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import unittest
import logging
import io

import antlr4

import gminor_visitor
import symbols
from lib import toplevel
from gminor_lexer import gminorLexer as GminorLexer
from gminor_parser import gminorParser as GminorParser


def configure_logging():
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.CRITICAL)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.CRITICAL)
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    console_handler.setFormatter(formatter)
    root_logger.addHandler(console_handler)


class TestGminorParser(unittest.TestCase):
    def setup(self, text):
        lexer = GminorLexer(antlr4.InputStream(text))
        stream = antlr4.CommonTokenStream(lexer)
        parser = GminorParser(stream)

        return parser

    def create_program_visitor(self, text):
        """
        Setup the parser and return a GminorVisitor instance on the given text
        that begins with the `program` production along with a parse tree.

        """

        parser = self.setup(text)
        tree = parser.program()
        output = io.StringIO()
        toplevel_namespace = toplevel.create_toplevel_namespace()
        symbol_table = symbols.SymbolTable(toplevel_namespace)
        visitor = gminor_visitor.GminorVisitor(symbol_table)
        return visitor, tree

    def test_procedure(self):
        text = '(lambda (x y) (+ x y))'
        parser = self.setup(text)
        tree = parser.procedure()
        output = io.StringIO()
        toplevel_namespace = toplevel.create_toplevel_namespace()
        symbol_table = symbols.SymbolTable(toplevel_namespace)
        visitor = gminor_visitor.GminorVisitor(symbol_table)
        visitor.visit(tree)

    def test_program(self):
        text = ('(define x 1)\n'
                '(define y 2)\n'
                '(define add-y (lambda (a)(+ a y)))\n'
                '(define result (add-y x))\n'
                'result\n')

        visitor, tree = self.create_program_visitor(text)
        visitor.visit(tree)

    def test_unbound_variable(self):
        text = ('(define x 1)\n'
                '(define y 2)\n'
                '(define add-y (lambda (a)(+ a z)))\n'
                '(define result (add-y x))\n'
                'result\n')

        visitor, tree = self.create_program_visitor(text)
        self.assertRaises(symbols.NameNotFoundError, visitor.visit, tree)

    def test_syntax_error_missing_parenthesis(self):
        text = ('(define x 1)\n'
                '(define y 2)\n'
                '(define add-y (lambda (a)(+ a y))\n'
                '(define result (add-y x))\n'
                'result\n')

        visitor, tree = self.create_program_visitor(text)
        visitor.visit(tree)

    def test_syntax_error_extraneous_parenthesis(self):
        text = ('(define x 1)\n'
                '(define y 2)\n'
                '(define add-y (lambda (a)(+ a y)))\n'
                '(define result (add-y x)))\n'
                'result\n')

        visitor, tree = self.create_program_visitor(text)
        visitor.visit(tree)

    def test_syntax_error_missing_parameter_list(self):
        text = ('(define x 1)\n'
                '(define y 2)\n'
                '(define add-y (lambda (+ a y)))\n'
                '(define result (add-y x))\n'
                'result\n')

        visitor, tree = self.create_program_visitor(text)
        visitor.visit(tree)

    @unittest.expectedFailure
    def test_visit_Procedure_call(self):
        text = ('(define x 1)\n'
                '(define y 2)\n'
                '(define add-y (lambda (a)(+ a y)))\n'
                '(define result (add-y x))\n'
                'result\n')

        visitor, tree = self.create_program_visitor(text)
        visitor.visit(tree)
        result_symbol = visitor._symbol_table._namespace_stack[1].symbols['result']
        self.assertIsNotNone(result_symbol.value)
        self.assertTrue(isinstance(result_symbol.type_, gminor.IntegerType))


if __name__ == '__main__':
    configure_logging()
    unittest.main()
