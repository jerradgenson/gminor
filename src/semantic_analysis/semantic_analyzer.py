"""
Defines the Gminor abstract syntax tree visitor and related functions.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import logging

from semantic_analysis import symbols
from semantic_analysis import gminor_types
from syntax_analysis.gminor_parser import gminorParser as GminorParser
from syntax_analysis.base_visitor import gminorVisitor as BaseVisitor


class SemanticAnalyzer(BaseVisitor):
    """
    Defines nodes in the abstract syntax tree along with the semantic actions to
    take for each of them.

    """

    def __init__(self, program_name, symbol_table):
        self._logger = logging.getLogger(__name__)
        self.symbol_table = symbol_table
        self.program_name = program_name

    # Visit a parse tree produced by GminorParser#program.
    def visitProgram(self, ctx: GminorParser.ProgramContext):
        child_return_values = self.visitChildren(ctx)
        return child_return_values

    # Visit a parse tree produced by GminorParser#variable_binding.
    def visitVariable_binding(self, ctx: GminorParser.Variable_bindingContext):
        id_ = ctx.user_identifier().getText()
        self._logger.debug("Binding to symbol '%s' on line %s", id_, ctx.start.line)
        expr_symbol = self.visitS_expression(ctx.s_expression(), id_)
        if expr_symbol:
            var_symbol = symbols.SymbolDescriptor(id_,
                                                  value=expr_symbol.value,
                                                  type_=expr_symbol.type)

        else:
            # TODO: remove this branch when visit_Procedure_call() is implemented.
            # symbol will be None when the child s-expression is a procedure call
            # until visit_Procedure_call() has been implemented.
            var_symbol = symbols.SymbolDescriptor(id_)

        self.symbol_table.add_symbol(var_symbol)

    # Visit a parse tree produced by GminorParser#integer.
    def visitInteger_literal(self, ctx: GminorParser.Integer_literalContext):
        text = ctx.getText()
        return symbols.SymbolDescriptor(value=text,
                                        type_=gminor_types.IntegerType())

    # Visit a parse tree produced by GminorParser#float_.
    def visitFloat_literal(self, ctx: GminorParser.Float_literalContext):
        text = ctx.getText()
        return symbols.SymbolDescriptor(value=text,
                                        type_=gminor_types.FloatType())

    # Visit a parse tree produced by GminorParser#boole.
    def visitBoolean_literal(self, ctx: GminorParser.Boolean_literalContext):
        text = ctx.getText()
        return symbols.SymbolDescriptor(value=text,
                                        type_=gminor_types.BooleanType())

    # Visit a parse tree produced by GminorParser#string_literal.
    def visitString_literal(self, ctx: GminorParser.String_literalContext):
        return symbols.SymbolDescriptor(value=ctx.getText(),
                                        type_=gminor_types.StringType())

    # Visit a parse tree produced by GminorParser#identifier.
    def visitIdentifier(self, ctx: GminorParser.IdentifierContext):
        id_ = ctx.getText()
        self._logger.debug("Reference to symbol '%s' on line %s", id_, ctx.start.line)
        return self.symbol_table.find_symbol(id_)

    # Visit a parse tree produced by GminorParser#user_identifier.
    def visitUser_identifier(self, ctx: GminorParser.User_identifierContext):
        # visitUser_identifier should behave the same as visitIdentifier.
        return self.visitIdentifier(ctx)

    # Visit a parse tree produced by GminorParser#procedure.
    def visitProcedure(self, ctx: GminorParser.ProcedureContext, id_=None):
        label = symbols.gen_label()
        symbol = symbols.SymbolDescriptor(value=label,
                                          type_=gminor_types.ProcedureType(None))

        if id_:
            namespace_name = id_ + ' ' + label
            symbol.id = id_
            self.symbol_table.add_symbol(symbol)

        else:
            namespace_name = label

        self.symbol_table.create_stack_frame(namespace_name,
                                             ctx.start.source[1].name,
                                             ctx.start.line)

        self.visitChildren(ctx)
        self.symbol_table.delete_stack_frame()
        return symbol

    # Visit a parse tree produced by GminorParser#parameter_list.
    def visitParameter_list(self, ctx: GminorParser.Parameter_listContext):
        for identifier in ctx.user_identifier():
            id_ = identifier.getText()
            symbol = symbols.SymbolDescriptor(id_, gminor_types.PolymorphicType())
            self.symbol_table.add_symbol(symbol)

    # Visit a parse tree produced by GminorParser#procedure_call.
    def visitProcedure_call(self, ctx: GminorParser.Procedure_callContext):
        procedure_symbol = self.visitS_expression(ctx.s_expression()[0])
        for expr in ctx.s_expression()[1:]:
            self.visitS_expression(expr)

        return symbols.SymbolDescriptor(type_=procedure_symbol.type.return_type)

    # Visit a parse tree produced by gminorParser#s_expression.
    def visitS_expression(self, ctx: GminorParser.S_expressionContext, id_=None):
        if ctx.procedure():
            symbol = self.visitProcedure(ctx.procedure(), id_)

        else:
            symbol = self.visitChildren(ctx)

        return symbol
    # Visit a parse tree produced by gminorParser#let_expression.
    def visitLet_expression(self, ctx: GminorParser.Let_expressionContext):
        self.symbol_table.create_stack_frame('let binding',
                                             ctx.start.source[1].name,
                                             ctx.start.line)

        for let_binding in ctx.let_binding():
            id_ = self.visitLet_binding(let_binding, get_id=True)
            symbol = symbols.SymbolDescriptor(id_, gminor_types.PolymorphicType())
            self.symbol_table.add_symbol(symbol)

        child_value = self.visitChildren(ctx)
        self.symbol_table.delete_stack_frame()

        return child_value

    # Visit a parse tree produced by gminorParser#let_binding.
    def visitLet_binding(self, ctx: GminorParser.Let_bindingContext, get_id=False):
        id_ = ctx.identifier().getText()
        if get_id:
            return id_

        else:
            self._logger.debug("Binding to symbol '%s' on line %s", id_, ctx.start.line)
            expr_symbol = self.visitS_expression(ctx.s_expression(), id_)
            if expr_symbol:
                var_symbol = symbols.SymbolDescriptor(id_,
                                                      value=expr_symbol.value,
                                                      type_=expr_symbol.type)

            else:
                # TODO: remove this branch when visit_Procedure_call() is implemented.
                # symbol will be None when the child s-expression is a procedure call
                # until visit_Procedure_call() has been implemented.
                var_symbol = symbols.SymbolDescriptor(id_)

            self.symbol_table.add_symbol(var_symbol)

    # Visit a parse tree produced by GminorParser#module_accessor.
    def visitModule_accessor(self, ctx: GminorParser.Module_accessorContext):
        ids = ctx.identifier()
        ids_string = '.'.join(i.getText() for i in ids)
        self._logger.debug("Reference to symbol '%s' on line %s", ids_string, ctx.start.line)

        mangled_names = dict()
        for count, id_ in enumerate(ids):
            id_text = id_.getText()
            mangled_name = mangled_names.get(id_text, id_text)
            if count == 0:
                _, namespace, mangled_names = self.symbol_table.find_symbol(mangled_name).value

            elif count < len(ids) - 1:
                _, namespace, mangled_names = namespace.find_symbol(mangled_name).value

            else:
                return namespace.find_symbol(mangled_name)


class GminorImportError(symbols.GminorError):
    def __str__(self):
        return 'Module not found'
