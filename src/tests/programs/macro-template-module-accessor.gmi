(import imath)
(define-syntax en-math (plus)
  ((x plus y) (+ x y imath.b)))

(define a (en-math 1 plus 2))
