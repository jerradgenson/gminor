(define-type btree btree integer poly btree)
(define btree-search
  (lambda (b i # j)
    (cond
     ((btree-empty? b)
      (cond
       ((not (opt-empty? j)) (opt-head j))
       (else (raise value-error (format "btree has no element '{}'." (int->str i))))))
     (else
      (let ((btree-index (btree-ref b 1)))
	(cond
	 ((= i btree-index) (btree-ref b 2))
	 ((> i btree-index) (btree-search (btree-right b) i j))
	 (else (btree-search (btree-left b) i j))))))))

(define btree-insert
  (lambda (b i v)
    (cond
     ((btree-empty? b) (btree-create i v))
     ((>= i (btree-ref b 1))
      (btree-rmod b (btree-insert (btree-right b) i v)))
     (else
      (btree-lmod b (btree-insert (btree-left b) i v))))))

(define btree-index (lambda (b) (btree-ref b 1)))
(define btree-right (lambda (b) (btree-ref b 3)))
(define btree-left (lambda (b) (btree-ref b 0)))
(define btree-rmod (lambda (b r) (btree-mod b 3 r)))
(define btree-lmod (lambda (b l) (btree-mod b 0 l)))
(define btree-create (lambda (i v) (btree (btree) i v (btree))))
