"""
Unit tests for syntax_analysis/macro_grammar.py.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import unittest

from pyparsing import ParseException, StringEnd

from syntax_analysis import macro_grammar


class TestSExpression(unittest.TestCase):
    def test_integer_literal(self):
        original = '1234567890'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], original)

    def test_float_literal(self):
        original = '-3.14159'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], original)

    def test_scinot_literal(self):
        original = '2.998e8'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], original)

    def test_boolean_literal(self):
        original = '#t'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], original)

    def test_string_literal1(self):
        original = r'"\"Wisely, and slow. They stumble that run fast.\""'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], original)

    def test_string_literal2(self):
        original = r"'An idea that\'s not dangerous is unworthy of being called an idea at all.'"
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], original)

    def test_identifier(self):
        original = 'test-var1'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], original)

    def test_procedure(self):
        original = '(lambda (x y # z) (+ x y))'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(list(result[0]),
                         ['(', 'lambda', '(', 'x', 'y', '#', 'z', ')', '(', '+', 'x', 'y', ')', ')'])

    def test_procedure_call(self):
        original = '(test-proc var1 2 (* var2 var3))'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(list(result[0]),
                         ['(', 'test-proc', 'var1', '2', '(', '*', 'var2', 'var3', ')', ')'])

    def test_cond_expression(self):
        original = '(cond ((> x 1) "greater than one") ((= x 1) \'equal to one\') (else "less than one"))'
        result = macro_grammar.s_expression().parseString(original)
        expected = ['(', 'cond', '(', '(', '>', 'x', '1', ')', '"greater than one"', ')', '(', '(', '=', 'x', '1', ')', "'equal to one'", ')', '(', 'else', '"less than one"', ')', ')']
        self.assertEqual(list(result[0]), expected)

    def test_let_expression(self):
        original = '(let ((a 1) (b 2) (c (+ a b))) c)'
        result = macro_grammar.s_expression().parseString(original)
        expected = ['(', 'let', '(', '(', 'a', '1', ')', '(', 'b', '2', ')', '(', 'c', '(', '+', 'a', 'b', ')', ')', ')', 'c', ')']
        self.assertEqual(list(result[0]), expected)

    def test_module_accessor(self):
        original = 'a.b.c'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], 'a.b.c')

    def test_reserved_identifier(self):
        original = '[a.b.c]'
        result = macro_grammar.s_expression().parseString(original)
        self.assertEqual(result[0][0], '[a.b.c]')

    def test_syntax_error_extraneous_l_paren(self):
        original = '(let ((a 1) (b 2) (c (+ a b))) (c)'
        parser = macro_grammar.s_expression() + StringEnd()
        self.assertRaises(ParseException, parser.parseString, original)

    def test_syntax_error_extraneous_r_paren1(self):
        original = '(let ((a 1) (b 2) (c (+ a b)))) c)'
        parser = macro_grammar.s_expression() + StringEnd()
        self.assertRaises(ParseException, parser.parseString, original)

    def test_syntax_error_extraneous_r_paren2(self):
        original = '(let ((a 1) (b 2) (c (+ a b))) c))'
        parser = macro_grammar.s_expression() + StringEnd()
        self.assertRaises(ParseException, parser.parseString, original)

    def test_syntax_error_missing_parens(self):
        original = 'lambda x y + x y'
        parser = macro_grammar.s_expression() + StringEnd()
        self.assertRaises(ParseException, parser.parseString, original)

    def test_syntax_error_invalid_characters(self):
        original = '(test-proc |var1| 2 (* var2 var3))'
        parser = macro_grammar.s_expression() + StringEnd()
        self.assertRaises(ParseException, parser.parseString, original)

    def test_syntax_error_invalid_float(self):
        original = '3.1415.9'
        parser = macro_grammar.s_expression() + StringEnd()
        self.assertRaises(ParseException, parser.parseString, original)
