# Generate lexer, parser, and base visitor files needed by Gminor.
# Usage: source build.sh [-c -i]
#
# Copyright 2020 Jerrad Michael Genson
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

build_dir=$(realpath '../build')
antlr_outdir=$build_dir/syntax_analysis
gminor_parsing=$build_dir/gminor_parsing
install_dir=/usr/lib/gminor

if [[ $1 == "--clean" || $1 == "-c" ]]; then
  # Clean the build directory.
  find $build_dir ! -name '.gitignore' ! -name 'build' -exec rm -fr {} +

elif [[ $1 == "--install" || $1 == "-i" ]]; then
  # Install Gminor on this system.
  [[ ! -d $install_dir ]] && mkdir $install_dir
  cp -R * $install_dir
  install_syntax=$install_dir
  install_syntax+="/syntax"
  cp -R $build_dir/gminor_parsing/. $install_syntax
  chmod -R 755 $install_dir
  echo "#!/bin/sh" > /usr/bin/gminor
  echo "/usr/bin/python3 $install_dir/gminor.py" ' "$@"'  >> /usr/bin/gminor
  chmod 755 /usr/bin/gminor

elif [[ $1 == "" ]];  then
  # Generate the files.
  antlr4 -Dlanguage=Python3 -visitor -o $build_dir 'syntax_analysis/gminor.g4'
  [[ ! -d $gminor_parsing ]] && mkdir $gminor_parsing
  mv $antlr_outdir/gminorLexer.py $gminor_parsing/gminor_lexer.py
  mv $antlr_outdir/gminorParser.py $gminor_parsing/gminor_parser.py
  sed -e 's/from \.gminorParser/from .gminor_parser/' -e 's/from gminorParser/from gminor_parser/' $antlr_outdir/gminorVisitor.py > $gminor_parsing/base_visitor.py
  sed -e 's/from \.gminorParser/from .gminor_parser/' -e 's/from gminorParser/from gminor_parser/' $antlr_outdir/gminorListener.py > $gminor_parsing/base_listener.py
  [[ ! -f $gminor_parsing/__init__.py ]] && touch $gminor_parsing/__init__.py
  rm -fr $antlr_outdir

else
  # Invalid option to build.sh
  echo "error: invalid option '$1'"
  echo "Usage: source build.sh [-c -i]"

fi
