"""
Defines built-in Gminor types and functions for manipulating types.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""


def is_type_equal(a, b):
    """
    Compare two Gminor types to determine if they are equal.

    """

    if isinstance(a, PrimitiveType):
        return type(a) == type(b)

    elif isinstance(a, ListType) and isinstance(b, ListType):
        return is_type_equal(a.element_type, b.element_type)

    elif isinstance(a, ProcedureType) and isinstance(b, ProcedureType):
        if len(a.parameters) != len(b.parameters):
            return False

        else:
            return (is_type_equal(a.return_type, b.return_type)
                    and all(is_type_equal(x, y) for x, y in zip(a.parameters, b.parameters)))

    elif isinstance(a, PolymorphicType) and isinstance(b, PolymorphicType):
        return a == b

    else:
        return False


class BaseType:
    __slots__ = ()


class PrimitiveType(BaseType):
    pass


class IntegerType(PrimitiveType):
    def __str__(self):
        return 'integer'


class FloatType(PrimitiveType):
    def __str__(self):
        return 'float'


class BooleanType(PrimitiveType):
    def __str__(self):
        return 'boolean'


class StringType(PrimitiveType):
    def __str__(self):
        return 'string'


class NilType(PrimitiveType):
    def __str__(self):
        return 'nil'


class CompoundType(BaseType):
    pass


class ListType(CompoundType):
    def __init__(self, element_type):
        self._element_type = element_type

    def __str__(self):
        return 'list'

    @property
    def element_type(self):
        return self._element_type


class ProcedureType(CompoundType):
    def __init__(self, return_type, **parameters):
        self._return_type = return_type
        self._parameters = parameters

    def __str__(self):
        return 'procedure'

    @property
    def return_type(self):
        return self._return_type

    @property
    def parameters(self):
        return self._parameters


class PolymorphicType(BaseType):
    def __str__(self):
        return 'polymorphic'


class NamespaceType(BaseType):
    pass
