"""
An integration test framework that calls different modules of a program
on test programs and compares the actual output with the contents of expected
output files.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import os
import sys
import traceback
import difflib
import inspect
from enum import Enum, auto
from collections import OrderedDict
from collections import namedtuple


def run(testcases, test_dir, silent=True):
    results = []
    status = TestStatus.PASSED
    for root, _, files in os.walk(test_dir):
        for input_file in files:
            input_path = os.path.join(root, input_file)
            for testcase in testcases:
                if os.path.splitext(input_file)[1] == testcase.input_suffix:
                    test_result = _check_output(testcase, input_path, silent)
                    if test_result[0] not in (TestStatus.PASSED, TestStatus.SKIPPED):
                        status = TestStatus.FAILED

                    results.append((testcase.name, os.path.splitext(input_file)[0]) + test_result)

    return status, results


def _check_output(testcase, input_path, silent):
    expected_output_file = os.path.splitext(input_path)[0] + testcase.output_suffix
    if not os.path.exists(expected_output_file):
        msg = str(expected_output_file) + ' not found'
        return TestStatus.NOT_IMPLEMENTED, msg

    try:
        orig_stdout = sys.stdout
        orig_stderr = sys.stderr
        with open(os.devnull, 'w') as null_stream:
            if silent:
                sys.stdout = null_stream
                sys.stderr = null_stream

            actual_output = testcase.func(input_path)

    except SkipTest as skip_test:
        return TestStatus.SKIPPED, str(skip_test)

    except Exception:
        msg = 'Exception raised during test:\n' + traceback.format_exc()
        return TestStatus.ERROR, msg

    finally:
        sys.stdout = orig_stdout
        sys.stderr = orig_stderr

    with open(expected_output_file) as expected_file_descriptor:
        expected_output = expected_file_descriptor.read()

    passed = actual_output == expected_output
    if passed:
        return TestStatus.PASSED, ''

    actual_lines = [x + '\n' for x in actual_output.split('\n')]
    expected_lines = [x + '\n' for x in expected_output.split('\n')]
    output_diff = ''.join(list(difflib.unified_diff(expected_lines,
                                                    actual_lines,
                                                    fromfile='expected output',
                                                    tofile='actual output')))

    msg = 'Actual output differs from expected output:\n' + output_diff.strip()
    return TestStatus.FAILED, msg


def collect_testcases(module):
    return getattr(module, '_testcases', list())


def format_results(results, verbose=False, skip_not_implemented=False):
    results_table = OrderedDict()
    for result in results:
        component_name = result[0]
        if component_name in results_table:
            results_table[component_name].append(result[1:])

        else:
            results_table[component_name] = [result[1:]]

    passed = 0
    failed = 0
    errors = 0
    skipped = 0
    not_implemented = 0
    total = 0
    formatted_text = ''
    for component_name, results in results_table.items():
        formatted_text += 'Test results for {}:\n'.format(component_name)
        for result in results:
            test_name = os.path.splitext(result[0])[0]
            test_status = result[1]
            test_details = result[2]
            if test_status == TestStatus.PASSED:
                passed += 1
                msg = test_name + '... passed\n'
                formatted_text += msg

            elif test_status == TestStatus.FAILED:
                failed += 1
                msg = test_name + '... failed'
                if verbose:
                    msg += '\n' + test_details

                formatted_text += msg + '\n'

            elif test_status == TestStatus.ERROR:
                errors += 1
                msg = test_name + '... error\n' + test_details
                formatted_text += msg

            elif test_status == TestStatus.SKIPPED:
                skipped += 1
                msg = test_name + '... skipped'
                if verbose:
                    msg += ' ({})'.format(test_details)

                formatted_text += msg + '\n'

            elif test_status == TestStatus.NOT_IMPLEMENTED:
                if skip_not_implemented:
                    continue

                not_implemented += 1
                msg = test_name + '... not implemented'
                if verbose:
                    msg += ' ({})'.format(test_details)

                formatted_text += msg + '\n'

            else:
                err = 'invalid test status: {}'.format(test_status)
                raise ValueError(err)

        formatted_text += '\n'

    total = passed + failed + errors + skipped + not_implemented
    formatted_text += 'Total tests passed:    {}\n'.format(passed)
    formatted_text += 'Total tests failed:    {}\n'.format(failed)
    formatted_text += 'Total test errors:     {}\n'.format(errors)
    formatted_text += 'Total tests skipped:   {}\n'.format(skipped)
    formatted_text += 'Total not implemented: {}\n'.format(not_implemented)
    formatted_text += 'Total tests run:       {}'.format(total)

    return formatted_text


def testcase(input_suffix, output_suffix, name=''):
    def create_testcase(f):
        testcase_f = Testcase(f,
                              name if name else f.__name__,
                              input_suffix,
                              output_suffix)

        module = inspect.getmodule(f)
        if not hasattr(module, '_testcases'):
            module._testcases = []

        module._testcases.append(testcase_f)

        return f

    return create_testcase


def generate_output(testcases, test_name, test_file):
    for testcase in testcases:
        if testcase.name == test_name:
            return testcase.func(test_file)

    raise ValueError("Test case '{}' not found.".format(test_name))


Testcase = namedtuple('Testcase', ('func',
                                   'name',
                                   'input_suffix',
                                   'output_suffix'))


class TestStatus(Enum):
    PASSED = auto()
    FAILED = auto()
    ERROR = auto()
    SKIPPED = auto()
    NOT_IMPLEMENTED = auto()


class SkipTest(Exception):
    pass
