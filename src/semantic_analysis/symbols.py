"""
Contains SymbolTable, SymbolDescriptor, and StackFrame definitions.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import logging
from collections import namedtuple


def _create_gen_label():
    """
    Return a function for generating Gminor Bytecode labels that are unique within
    the scope of the returned function.

    """

    current = 0
    def gen_label():
        nonlocal current
        label = '[L' + str(current) + ']'
        current += 1
        return label

    return gen_label


gen_label = _create_gen_label()


class SymbolDescriptor:
    def __init__(self, id_=None, type_=None, value=None):
        self.id = id_
        self.type = type_
        self.value = value

    def __str__(self):
        return self.id


class SymbolTable:
    STACK_TRACE_TEMPLATE = '  File "{file}", line {line}, in {stack_frame}'

    def __init__(self, global_frame=None, silent=False):
        self._stack = []
        self._logger = logging.getLogger(__name__)
        if silent:
            self._logger.setLevel(logging.WARNING)

        if global_frame:
            self._stack.append(global_frame)

    def create_stack_frame(self, name, file, line):
        self._stack.append(StackFrame(name, file, line, dict()))
        self._logger.debug('Stack frame created')

    def add_symbol(self, symbol):
        self._stack[-1].symbols[symbol.id] = symbol

    def find_symbol(self, id_):
        for stack_frame in reversed(self._stack):
            if id_ in stack_frame.symbols:
                symbol = stack_frame.symbols[id_]
                return symbol

        raise NameNotFoundError(self.get_stack_trace(), id_)

    def delete_stack_frame(self):
        self._stack.pop()
        self._logger.debug('Stack frame deleted')

    def get_stack_trace(self):
        stack_trace = []
        for stack_frame in self._stack:
            if stack_frame.name != '__main__':
                stack_trace_line = self.STACK_TRACE_TEMPLATE.format(stack_frame=stack_frame.name,
                                                                    file=stack_frame.file,
                                                                    line=stack_frame.line)

                stack_trace = [stack_trace_line] + stack_trace

        return stack_trace

    def __getstate__(self):
        return dict(_stack=self._stack,
                    _silent=True)


StackFrame = namedtuple('StackFrame', ('name', 'file', 'line', 'symbols'))


class GminorError(Exception):
    def __init__(self, stack_trace, info):
        self.stack_trace = stack_trace
        self.info = info

    def __str__(self):
        return self.info


class NameNotFoundError(GminorError):
    def __str__(self):
        return 'Name not found'


class GminorSyntaxError(GminorError):
    def __str__(self):
        return 'Syntax error'


class GminorImportError(GminorError):
    def __str__(self):
        return 'Import error'
