"""
Tools for parsing user-defined syntactic macros.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import re
from pathlib import Path
from functools import lru_cache

from pyparsing import (
    Word,
    Literal,
    Regex,
    alphas,
    nums,
    alphanums,
    Optional,
    ZeroOrMore,
    OneOrMore,
    Forward,
    Group,
    Combine,
)

GRAMMAR_FILE = Path('syntax_analysis') / Path('gminor.g4')


# Parse SPECIAL fragment regexp from antlr grammar.
def _get_specials():
    with GRAMMAR_FILE.open() as grammar_file_descriptor:
        grammar = grammar_file_descriptor.read()

    specials = re.search(r'SPECIAL\s*:\s*\[([^\]]+)\]\s*;', grammar).group(1)

    return specials


@lru_cache(maxsize=None)
def s_expression():
    """
    Construct a pattern that matches s-expressions in syntax rules defined by
    the user with define-syntax.

    Returns
      A pyparsing ParserElement that matches a single s-expression.

    """

    # (terminal) special (based on gminor.g4)
    special = Word(_get_specials()) | '.' | '#' | '[' | ']' | '{' | '}'

    # (terminal) boolean
    boolean = Literal('#t') | Literal('#f')

    int_literal = OneOrMore(Word(nums))

    # (terminal) float
    float_literal = Combine(Optional('-') + (ZeroOrMore(Word(nums)) + '.' + int_literal) | (int_literal + '.'))

    # (terminal) sci_notation
    scinot_literal = Combine(float_literal + 'e' + int_literal)

    # (terminal) string
    string_literal = (Combine(('"' + ZeroOrMore(Regex(r'((\\")|[^"])')) + '"'))
                      | Combine(("'" + ZeroOrMore(Regex(r"((\\')|[^'])")) + "'")))

    # (terminal) identifier
    identifier = Combine((Word(alphas) | special) + ZeroOrMore(Word(alphanums) | special))

    # (nonterminal) atom : special | alphanums | boolean
    atom = scinot_literal | float_literal | int_literal | identifier | boolean | string_literal

    # (nonterminal) s_expression : atom | '(' s_expression ')'
    s_expression_ = Forward()
    s_expression_ <<= atom | ('(' + ZeroOrMore(s_expression_) + ')')
    s_expression_ = Group(s_expression_)

    return s_expression_
