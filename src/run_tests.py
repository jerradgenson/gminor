"""
Copyright 2020 Jerrad Michael Genson
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import os
import sys
import time
import argparse
import unittest

import integration
from tests import integration_tests

TEST_PROGRAMS = 'tests/programs'
UNIT_TEST_DIR = 'tests/unit'


def main():
    start_time = time.time()
    clargs = parse_command_line()
    integration_testcases = integration.collect_testcases(integration_tests)
    if clargs.generate_output:
        print(integration.generate_output(integration_testcases,
                                          clargs.generate_output[0],
                                          clargs.generate_output[1]))

    else:
        print('Unit Testcases')
        print('--------------')
        unit_testcases = unittest.TestLoader().discover(UNIT_TEST_DIR,
                                                        top_level_dir=os.getcwd())

        unit_test_runner = unittest.TextTestRunner(stream=sys.stdout, verbosity=2)
        unit_test_results = unit_test_runner.run(unit_testcases)
        unit_test_failing = unit_test_results.failures or unit_test_results.errors

        print('\nIntegration Testcases')
        print('---------------------')
        _, results = integration.run(integration_testcases,
                                     TEST_PROGRAMS,
                                     silent=clargs.silent and not clargs.verbose)

        status_not_passing = integration.TestStatus.ERROR, integration.TestStatus.FAILED
        integration_tests_failing = [r for r in results if r[2] in status_not_passing]
        formatted_result = integration.format_results(results,
                                                      verbose=clargs.verbose,
                                                      skip_not_implemented=clargs.skip_not_implemented)

        print(formatted_result)
        is_passing = not (unit_test_failing or integration_tests_failing)
        if is_passing:
            print('\nAll tests are passing')
            exit_code = 0

        else:
            print('\nTests not passing')
            exit_code = 1

        print('\nRan test suite in {:.3f} seconds'.format(time.time() - start_time))

        return exit_code


def parse_command_line():
    parser = argparse.ArgumentParser(description='Run unit and integration test cases.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Run tests in verbose mode. Print detailed information about test results.')

    parser.add_argument('--silent', action='store_true',
                        help='Run tests in silent mode. Suppress printing to stdout and stderr during a test run. This option is ignored if used with --verbose.')

    parser.add_argument('--skip-not-implemented', action='store_true',
                        help='Skip showing results for test cases that are not implemented.')

    parser.add_argument('--generate-output',
                        nargs=2,
                        help='Takes a testcase name and an input file and generates output data.')

    clargs = parser.parse_args()

    return clargs


if __name__ == '__main__':
    sys.exit(main())
