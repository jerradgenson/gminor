/*
Defines a Gminor lexer and parser specification for use with ANTLR 4.

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

*/

grammar gminor;

/********************/
/*   Parser Rules   */
/********************/
program             : (variable_binding | type_definition | s_expression | macro_expression | module_import | import_from)* EOF ;
import_from         : L_PAREN IMPORT_FROM user_identifier user_identifier+ R_PAREN ;
module_import       : L_PAREN IMPORT user_identifier user_identifier? R_PAREN ;
module_accessor     : identifier (DOT identifier)+ ;
macro_expression    : L_PAREN DEFINE_SYNTAX user_identifier keyword_list syntax_rule+ R_PAREN ;
keyword_list        : L_PAREN user_identifier* R_PAREN ;
syntax_rule         : L_PAREN L_PAREN s_expression* R_PAREN macro_template R_PAREN ;
macro_template      : variable_binding | type_definition | s_expression ;
type_definition     : L_PAREN DEFINE_TYPE user_identifier (identifier | POLY)+ R_PAREN ;
variable_binding    : L_PAREN DEFINE user_identifier s_expression R_PAREN ;
s_expression        : atom | module_accessor | procedure | cond_expression | let_expression | procedure_call ;
procedure           : L_PAREN LAMBDA parameter_list s_expression* R_PAREN ;
parameter_list      : L_PAREN user_identifier* (HASH user_identifier)? R_PAREN ;
let_expression      : L_PAREN LET L_PAREN let_binding* R_PAREN s_expression+ R_PAREN ;
let_binding         : L_PAREN identifier s_expression R_PAREN ;
cond_expression     : L_PAREN COND cond_subexpression+ else_clause? R_PAREN ;
cond_subexpression  : L_PAREN s_expression s_expression? R_PAREN ;
else_clause         : L_PAREN ELSE s_expression+ R_PAREN ;
procedure_call      : L_PAREN s_expression+ R_PAREN ;
atom                : integer_literal | float_literal | boolean_literal | string_literal | identifier ;
identifier          : user_identifier | (L_SQUARE user_identifier R_SQUARE) ;
user_identifier     : IDENTIFIER ;
string_literal      : STRING ;
boolean_literal     : TRUE | FALSE ;
float_literal       : FLOAT | SCI_NOTATION ;
integer_literal     : INTEGER ;


/*******************/
/*   Lexer Rules   */
/*******************/

/* fragments */
fragment DIGIT   : [0-9] ;
fragment LETTER  : [a-zA-Z] ;
fragment SPECIAL : [~!@$%^&*\\\-/_+=?<>] ;

/* keywords */
DEFINE_SYNTAX    : 'define-syntax' ;
DEFINE_TYPE      : 'define-type' ;
SYNTAX_RULES     : 'syntax-rules' ;
DEFINE           : 'define' ;
LAMBDA           : 'lambda' ;
LET              : 'let' ;
COND             : 'cond' ;
ELSE             : 'else' ;
IMPORT_FROM      : 'import-from' ;
IMPORT           : 'import' ;
POLY             : 'poly' ;

/* data types */
SCI_NOTATION     : FLOAT 'e' INTEGER ;
FLOAT            : '-'? (DIGIT* '.' DIGIT+) | (DIGIT+ '.') ;
INTEGER          : '-'? DIGIT+ ;
TRUE             : '#t' ;
FALSE            : '#f' ;
STRING           : ('"' ('\\"' | ~'"')* '"') | ('\'' ('\\\'' | ~'\'')* '\'') ;

/* identifiers */
IDENTIFIER       : (LETTER | SPECIAL) (LETTER | DIGIT | SPECIAL)* ;

/* syntactic elements */
L_PAREN          : '(' ;
R_PAREN          : ')' ;
DOT              : '.' ;
ELLIPSIS         : '...' ;
HASH             : '#' ;
L_SQUARE          : '[' ;
R_SQUARE          : ']' ;

/* comments */
COMMENT          : ';' ~'\n'* -> skip ;

/* whitespace */
WHITESPACE       : (' ' | [\n\t\r]) -> skip ;
