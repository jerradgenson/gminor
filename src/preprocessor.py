"""
Defines the Gminor preprocessor and related functions.

The preprocessor traverses a parse tree produced from gminor.g4, compiles modules
referenced by the parse tree, imports their symbol tables, performs macro
transformations on the parse tree, and generates new Gminor source code with the
macro transformations applied. The preprocessor accumulates macro symbols (from
both the parse tree and imported modules) in `Preprocessor.symbol_table` and
regular symbols in `Preprocessor.imported_symbols`.

For example, given the Gminor programs isub.gminor and idd.gminor:

;; isub.gminor
(define-syntax isub (-)
  ((x - y)
   (- x y)))
(define add1 (lambda (x) (+ x 1)))

;; iadd.gminor
(import-from isub isub add1)
(define-syntax iadd (+)
  ((x + y)
   (+ x y)))
(iadd 1 + 2)
(isub 1 - 2)
(add1 2)

The preprocessor will produce the following output for iadd.gminor:

(+ 1 2)
(- 1 2)
([isub-0].add1 2)

Copyright (C) 2020 Jerrad Michael Genson

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""

import os
import re
import pickle
import logging
from itertools import chain

from pyparsing import Literal, StringEnd, empty

from semantic_analysis import symbols
from semantic_analysis import gminor_types
from syntax_analysis.macro_grammar import s_expression
from syntax_analysis.gminor_parser import gminorParser as GminorParser
from syntax_analysis.base_visitor import gminorVisitor as BaseVisitor

# Amount of whitespace to use per indentation level when pretty-printing code.
INDENT = '  '


def reconstruct(s_expr):
    """
    Reconstruct the original s-expressions from a pyparsing Results object
    matched by S_EXPRESSION.

    Args
      s_expr: A pyparsing Results object matched by S_EXPRESSION.

    Returns
      A string representation of the s-expression.

    """

    if s_expr[0] != '(':
        # The s-expression is an atom.
        return ''.join(s_expr[0])

    text = ''
    for count, char in enumerate(s_expr):
        if count > 0 and text[-1] != '(' and char != ')':
            text += ' '

        text += char

    return text


def find_module(name, working_directory):
    """
    Locate the target module and return a path to the corresponding source file.
    If the target module is not found, return an empty string.

    Args
      name: The name of the module to find.
      working_directory: The working directory of the program currently being compiled.

    Returns
      The absolute path to the source file as a string, or an empty string.

    """

    source_file_name = name + '.sym'
    directories = [working_directory]
    directory_string = os.environ.get('COREPATH')
    directories.extend(directory_string.split(':') if directory_string else list())
    for dirpath, filenames in chain((dirpath, os.listdir(dirpath)) for dirpath in directories):
        for filename in filenames:
            if filename == source_file_name:
                return os.path.join(dirpath, filename)

    return ''


def load_module(path):
    """
    Load macro symbols and regular symbols from a Gminor module at `path`.

    Args
      path: The path to the Gminor module to load symbols from, as a string.

    Returns
      A two-tuple of symbol tables - the first of which is for macro symbols,
      and the second of which is for regular symbols.

    """

    with open(path, 'rb') as input_file:
        module_symbols = pickle.load(input_file)

    return module_symbols


def unbox_semantic_element(semantic_element, text, parser_element, symbol_descriptor):
    """
    Safely unbox the value encapsulated by a SemanticElement object.
    Accepts a SemanticElement object and three positive integer arguments
    representing the corresponding SemanticElement attributes. Attributes with
    an integer value of 0 are not tried. All other attributes are tried in
    reverse numerical order.

    Args
      semantic_element: An instance of SemanticElement.
      text: An integer indicating whether or not to try unboxing a text value
            and in what order.
      parser_element: An integer indicating whether or not to try unboxing a
                      parser_element value and in what order.
      symbol_descriptor: An integer indicating whether or not to try unboxing a
                         symbol_descriptor value and in what order.

    Returns
      One of SemanticElement.text, parser_element, or symbol_descriptor.

    Raises
      AttributeError if None of the expected SemanticElement attributes exist.

    """

    if text + parser_element + symbol_descriptor <= 0:
        raise ValueError('No attribute arguments given are greater than 0.')

    attribute_order = [(text, 'text'),
                       (parser_element, 'parser_element'),
                       (symbol_descriptor, 'symbol_descriptor')]

    attribute_order.sort(key=lambda a: a[0])
    attribute_order = [a[1] for a in attribute_order if a[0] > 0]
    for attribute in attribute_order:
        try:
            return getattr(semantic_element, attribute)

        except AttributeError as ae:
            attribute_error = ae

    raise attribute_error


def apply_macro(syntax_rules, s_expressions):
    """
    Apply the syntax rules for a macro to the target s-expressions.

    Args
      syntax_rules: An iterable of (pyparsing expression, template string) tuples.
                    The pyparsing expression corresponds to a custom production
                    rule defined by the user as a define-syntax macro, and it
                    matches s-expressions conforming to that rule. The template
                    string specifies how to apply the macro transformation by
                    defining placeholder variables that correspond to group
                    names in the pyparsing expression. When a pyparsing expression
                    in syntax_rules matches s_expressions, apply_macro substitutes
                    the text in the pyparsing expression groups for the template
                    string's placeholder variables.

      s_expressions: A string of s-expressions to apply the macro transformation to.

    Returns
      A SemanticElement object representing s-expressions that have been
      transformed by the syntax_rules.

    """

    # Check all the patterns in syntax rules against the target s-expressions.
    for pattern, template in syntax_rules:
        if (pattern + StringEnd()).matches(s_expressions):
            results = pattern.parseString(s_expressions)

            # Get pattern variables and the parsed s-expressions they correspond to.
            pattern_variables = {key: reconstruct(value[0]) for key, value in results.asDict().items()}

            # Format the macro template string using pattern variables,
            # thereby generating transformed s-expressions.
            transformed_s_expressions = template.format(**pattern_variables)
            return SemanticElement('apply_macro', text=transformed_s_expressions)

    # None of the syntax rules match the target s-expressions.
    # This indicates an incorrect application of the macro.
    raise symbols.GminorSyntaxError(tuple(), '')


def mangle_name(module_name, module_symbol=None):
    """
    Apply name-mangling to a symbol from an imported module so that the
    scoping will be correct in the compilation phase.

    If the module as a whole is being imported, then the module name will
    simply be returned surrounded by square brackets. If a particular
    symbol from the module is being imported, then the symbol name will
    be prefixed by the module name and an undersgminor, and both will be
    surrounded by square brackets.

    This name-mangling is performed to transform the identifier into one
    that is impossible for the user to define, thus guaranteeing that they
    can not access it before the line where the import occurred, even
    though the symbol will be present in the symbol table at the beginning
    of the compilation phase.

    Args
      module_name: The name of the module from which a symbol is being
                   imported from.
      module_symbol: The name of the symbol that is being imported. If
                     the module as a whole is being imported (as in
                     (import module)), this should be given the default
                     value of `None`.

    Returns
      Name of the imported symbol with the name-mangling algorithm applied.

    """

    if module_symbol:
        if module_symbol[0] == '[':
            module_symbol = module_symbol[1:-1]

        return '[{}_{}]'.format(module_name, module_symbol)

    return '[{}]'.format(module_name)


class Preprocessor(BaseVisitor):
    """
    Traverses the parse tree of a Gminor program and executes semantic actions
    for the preprocessor. Applies macro transformations, compiles imported
    modules, and aggregates symbols from those modules.

    Args
      program_name: Name of the module to preprocess.
      working_directory: Path to the module's working directory.

    """

    def __init__(self, program_name, working_directory):
        self._logger = logging.getLogger(__name__)
        self._working_directory = working_directory

        # Tracks the current amount of indentation for pretty-printing.
        self._indent = ''

        # Maps identifier names in the source text to alternate names to substitute in.
        # We need to do this for identifiers that reference modules to achieve proper
        # lexical scoping; otherwise, module variables will exhibit undesirable behavior,
        # such as being valid in code prior to where they were imported.
        self.mangled_names = {}
        self.program_name = program_name

        # Symbol table for macro symbols from this module and imported modules.
        # Regular symbols from imported modules are stored in self.imported_symbols.
        # Regular symbols from this module are not stored by this visitor.
        self.symbol_table = symbols.SymbolTable(silent=True)

        # Holds the symbol tables from imported modules.
        self.imported_symbols = []

    def is_macro(self, id_):
        """
        Is the target id a macro in the symbol table?
        Return True if it is, False if it is not.

        """

        try:
            self.symbol_table.find_symbol(self.mangled_names.get(id_, id_))
            return True

        except symbols.NameNotFoundError:
            return False

    # Visit a parse tree produced by GminorParser#program.
    def visitProgram(self, ctx: GminorParser.ProgramContext):
        self.symbol_table.create_stack_frame('__main__',
                                             self.program_name,
                                             ctx.start.line)
        program = ''
        for child in ctx.children[:-1]:
            subprogram = self.visit(child)
            if subprogram.text.strip():
                program += subprogram.text + '\n'

        # Strip any stray whitespace before line endings.
        return '\n'.join(line.rstrip() for line in program.split('\n'))

    # Visit a parse tree produced by GminorParser#type_definition.
    def visitType_definition(self,
                             ctx: GminorParser.Type_definitionContext,
                             is_syntax_pattern=False,
                             literals=tuple(),
                             pattern_variables=tuple()):
        type_def = '('
        type_def += Literal('define-type') if is_syntax_pattern else 'define-type'
        variable_name = ctx.user_identifier().getText()
        # If there is a matching substitution name, remove it and use this
        # definition instead now.
        self.mangled_names.pop(variable_name, '')
        for count, child in enumerate(ctx.children[2:]):
            if count == 0 or hasattr(child, 'user_identifier'):
                identifier = self.visitIdentifier(child,
                                                  is_syntax_pattern=is_syntax_pattern,
                                                  literals=literals,
                                                  pattern_variables=pattern_variables)

                type_def += ' '
                type_def += unbox_semantic_element(identifier, 2, is_syntax_pattern, 0)

            elif ctx.parser.symbolicNames[child.getSymbol().type] == 'POLY':
                type_def += ' '
                type_def += Literal('poly') if is_syntax_pattern else 'poly'

            elif ctx.parser.symbolicNames[child.getSymbol().type] == 'R_PAREN':
                type_def += ')'

            else:
                raise SyntaxError('Preprocessor in undefined state.')

        if is_syntax_pattern:
            return SemanticElement('type_definition', parser_element=type_def)

        return SemanticElement('type_definition', text=type_def)

    # Visit a parse tree produced by GminorParser#variable_binding.
    def visitVariable_binding(self,
                              ctx: GminorParser.Variable_bindingContext,
                              is_syntax_pattern=False,
                              literals=tuple(),
                              pattern_variables=tuple()):

        variable_binding = '('
        variable_binding += Literal('define') if is_syntax_pattern else 'define '
        variable_name = ctx.user_identifier().getText()
        # If there is a matching substitution name, remove it and use this
        # definition instead now.
        self.mangled_names.pop(variable_name, '')
        identifier = self.visitIdentifier(ctx.user_identifier(),
                                          is_syntax_pattern=is_syntax_pattern,
                                          literals=literals,
                                          pattern_variables=pattern_variables)

        variable_binding += unbox_semantic_element(identifier, 2, is_syntax_pattern, 0)
        variable_binding += ' '
        semantic_element = self.visitS_expression(ctx.s_expression(),
                                                  is_syntax_pattern=is_syntax_pattern,
                                                  literals=literals,
                                                  pattern_variables=pattern_variables)

        s_expr = unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)
        variable_binding += s_expr + ')'
        if is_syntax_pattern:
            return SemanticElement('variable_binding', parser_element=variable_binding)

        return SemanticElement('variable_binding', text=variable_binding)

    # Visit a parse tree produced by GminorParser#macro_expression.
    def visitMacro_expression(self, ctx: GminorParser.Macro_expressionContext):
        macro_id = ctx.user_identifier().getText()
        self._logger.debug("Creating production rule for macro '%s' on line %s", macro_id, ctx.start.line)
        literals = self.visitKeyword_list(ctx.keyword_list())
        rules = (self.visitSyntax_rule(syntax_rule, literals) for syntax_rule in ctx.syntax_rule())
        text_rules = [(pattern, template.text) for pattern, template in rules]
        macro_symbol = symbols.SymbolDescriptor(id_=macro_id, value=text_rules)
        self.symbol_table.add_symbol(macro_symbol)

        return SemanticElement('macro_expression', text='')

    # Visit a parse tree produced by GminorParser#keyword_list.
    def visitKeyword_list(self, ctx: GminorParser.Keyword_listContext):
        return [id_.getText() for id_ in ctx.user_identifier()]

    # Visit a parse tree produced by GminorParser#syntax_rule.
    def visitSyntax_rule(self, ctx: GminorParser.Syntax_ruleContext, literals):
        # Accumulate and combine syntax pattern parser elements created with pyparsing.
        syntax_pattern = empty
        for s_expr in ctx.s_expression():
            semantic_element = self.visitS_expression(s_expr, is_syntax_pattern=True, literals=literals)
            syntax_pattern += unbox_semantic_element(semantic_element, 2, 1, 0)

        # Get pattern variables from the syntax pattern parser.
        pattern_variables = [expr.resultsName for expr in syntax_pattern.exprs if expr.resultsName]

        # Construct macro template string using pattern variables.
        macro_template = self.visitMacro_template(ctx.macro_template(), pattern_variables)

        return syntax_pattern, macro_template

    # Visit a parse tree produced by GminorParser#macro_template.
    def visitMacro_template(self, ctx: GminorParser.Macro_templateContext, pattern_variables):
        if ctx.variable_binding():
            return self.visitVariable_binding(ctx.variable_binding(),
                                              is_syntax_pattern=False,
                                              literals=tuple(),
                                              pattern_variables=pattern_variables)

        elif ctx.s_expression():
            return self.visitS_expression(ctx.s_expression(),
                                          is_syntax_pattern=False,
                                          literals=tuple(),
                                          pattern_variables=pattern_variables)

        elif ctx.type_definition():
            return self.visitType_definition(ctx.type_definition(),
                                             is_syntax_pattern=False,
                                             literals=tuple(),
                                             pattern_variables=pattern_variables)

        else:
            assert False

    # Visit a parse tree produced by GminorParser#s_expression.
    def visitS_expression(self,
                          ctx: GminorParser.S_expressionContext,
                          is_syntax_pattern=False,
                          literals=tuple(),
                          pattern_variables=tuple(),
                          is_procedure_call=False):
        """
        Visit an s-expression with arguments to indicate the context that the
        the s-expression is embedded in.

        Args
          is_syntax_pattern: True if the s-expression is part of a syntax pattern.
                             Defaults to False. This may not be True if pattern_variables
                             are given.
          literals: A sequence of string literals indicating keywords in a syntax rule.
                    Defaults to an empty tuple. It is an error if literals are given
                    while is_syntax_pattern is False.
          pattern_variables: A sequence of strings indicating which identifiers
                             are syntax pattern variables. Used when constructing
                             macro template strings. Defaults to an empty tuple.
                             This may not be given if is_syntax_pattern is True.
          is_procedure_call: Indicates s-expression is at the head of a procedure call.
                             Defaults to False.

        """

        # Check that literals are not given if is_syntax_pattern is False.
        if literals and not is_syntax_pattern:
            raise ValueError("'literals' given when 'is_syntax_pattern' is False.")

        # Check that is_syntax_pattern, is_procedure_call, and pattern_variables
        # are not simultaneously present/True.
        if is_syntax_pattern and pattern_variables:
            err = "'pattern_variables' given when 'is_syntax_pattern' is True."
            raise ValueError(err)

        if ctx.atom():
            s_expr = self.visitAtom(ctx.atom(), is_syntax_pattern, literals, pattern_variables, is_procedure_call)

        elif ctx.procedure():
            s_expr = self.visitProcedure(ctx.procedure(), is_syntax_pattern, literals, pattern_variables)

        elif ctx.procedure_call():
            s_expr = self.visitProcedure_call(ctx.procedure_call(), is_syntax_pattern, literals, pattern_variables)

        elif ctx.cond_expression():
            s_expr = self.visitCond_expression(ctx.cond_expression(), is_syntax_pattern, literals, pattern_variables)

        elif ctx.module_accessor():
            s_expr = self.visitModule_accessor(ctx.module_accessor())

        else:
            s_expr = self.visitLet_expression(ctx.let_expression(), is_syntax_pattern, literals, pattern_variables)

        return s_expr

    # Visit a parse tree produced by GminorParser#atom.
    def visitAtom(self,
                  ctx: GminorParser.AtomContext,
                  is_syntax_pattern=False,
                  literals=tuple(),
                  pattern_variables=tuple(),
                  is_procedure_call=False):

        if ctx.integer_literal():
            return self.visitInteger_literal(ctx.integer_literal(), is_syntax_pattern)

        elif ctx.float_literal():
            return self.visitFloat_literal(ctx.float_literal(), is_syntax_pattern)

        elif ctx.boolean_literal():
            return self.visitBoolean_literal(ctx.boolean_literal(), is_syntax_pattern)

        elif ctx.string_literal():
            return self.visitString_literal(ctx.string_literal(), is_syntax_pattern)

        else:
            return self.visitIdentifier(ctx.identifier(),
                                        is_syntax_pattern,
                                        literals,
                                        pattern_variables,
                                        is_procedure_call)

    # Visit a parse tree produced by GminorParser#integer_literal.
    def visitInteger_literal(self, ctx: GminorParser.Integer_literalContext, is_syntax_pattern=False):
        if is_syntax_pattern:
            return SemanticElement('integer_literal', parser_element=Literal(ctx.getText()))

        return SemanticElement('integer_literal', text=ctx.getText())

    # Visit a parse tree produced by GminorParser#float_literal.
    def visitFloat_literal(self, ctx: GminorParser.Float_literalContext, is_syntax_pattern=False):
        if is_syntax_pattern:
            return SemanticElement('float_literal', parser_element=Literal(ctx.getText()))

        return SemanticElement('float_literal', text=ctx.getText())

    # Visit a parse tree produced by GminorParser#boolean_literal.
    def visitBoolean_literal(self, ctx: GminorParser.Boolean_literalContext, is_syntax_pattern=False):
        if is_syntax_pattern:
            return SemanticElement('boolean_literal', parser_element=Literal(ctx.getText()))

        return SemanticElement('boolean_literal', text=ctx.getText())

    # Visit a parse tree produced by GminorParser#string_literal.
    def visitString_literal(self, ctx: GminorParser.String_literalContext, is_syntax_pattern=False):
        if is_syntax_pattern:
            return SemanticElement('string_literal', parser_element=Literal(ctx.getText()))

        return SemanticElement('string_literal', text=ctx.getText())

    # Visit a parse tree produced by GminorParser#identifier.
    def visitIdentifier(self,
                        ctx: GminorParser.IdentifierContext,
                        is_syntax_pattern=False,
                        literals=tuple(),
                        pattern_variables=tuple(),
                        is_procedure_call=False,
                        suppress_substitution=False):

        text = ctx.getText()
        if is_syntax_pattern and text in literals:
            return SemanticElement('identifier', parser_element=Literal(text))

        elif is_syntax_pattern:
            return SemanticElement('identifier', parser_element=s_expression().setResultsName(text, True))

        elif text in pattern_variables:
            text = '{' + text + '}'
            return SemanticElement('identifier', text=text)

        elif is_procedure_call and self.is_macro(text):
            syntax_rules = self.symbol_table.find_symbol(self.mangled_names.get(text, text))
            return SemanticElement('identifier', symbol_descriptor=syntax_rules)

        elif suppress_substitution:
            return SemanticElement('identifier', text=text)

        else:
            return SemanticElement('identifier', text=self.mangled_names.get(text, text))

    # Visit a parse tree produced by GminorParser#procedure_call.
    def visitProcedure_call(self,
                            ctx: GminorParser.Procedure_callContext,
                            is_syntax_pattern=False,
                            literals=tuple(),
                            pattern_variables=tuple()):
        procedure_call = '('
        s_expressions = []
        for count, s_expr in enumerate(ctx.s_expression()):
            s_expressions.append(self.visitS_expression(s_expr,
                                                        is_syntax_pattern=is_syntax_pattern,
                                                        literals=literals,
                                                        pattern_variables=pattern_variables,
                                                        is_procedure_call=count == 0))

        try:
            macro = s_expressions[0].symbol_descriptor
            syntax_patterns = macro.value
            self._logger.debug("Applying macro '%s' on line %s", macro.id, ctx.start.line)
            if len(s_expressions) == 1:
                expression_str = ''

            else:
                expression_str = ' '.join(s.text for s in s_expressions[1:])

            try:
                semantic_element = apply_macro(syntax_patterns, expression_str)

            except symbols.GminorSyntaxError as gminor_syntax_error:
                err = "line {} column {} - target expression does not match any pattern in macro '{}'"
                err = err.format(ctx.start.line, ctx.start.column, macro.id)
                gminor_syntax_error.info = err
                raise gminor_syntax_error

            transformed_s_expressions = unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)
            if is_syntax_pattern:
                return SemanticElement('procedure_call', parser_element=transformed_s_expressions)

            return SemanticElement('procedure_call', text=transformed_s_expressions)

        except AttributeError as attribute_error:
            if "does not have 'symbol_descriptor'" not in str(attribute_error):
                raise

            procedure_call += ' '.join(unbox_semantic_element(s, 2, is_syntax_pattern, 0) for s in s_expressions)
            procedure_call += ')'
            if is_syntax_pattern:
                return SemanticElement('procedure_call', parser_element=procedure_call)

            return SemanticElement('procedure_call', text=procedure_call)

    # Visit a parse tree produced by GminorParser#parameter_list.
    def visitParameter_list(self,
                            ctx: GminorParser.Parameter_listContext,
                            is_syntax_pattern=False,
                            literals=tuple(),
                            pattern_variables=tuple()):

        parameter_list = '('
        for count, identifier in enumerate(ctx.user_identifier()):
            if count > 0:
                parameter_list += ' '

            if ctx.HASH() and count == len(ctx.user_identifier()) - 1:
                parameter_list += '# '

            semantic_element = self.visitIdentifier(identifier, is_syntax_pattern, literals, pattern_variables)
            parameter_list += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)

        parameter_list += ')'
        if is_syntax_pattern:
            return SemanticElement('parameter_list', parser_element=parameter_list)

        return SemanticElement('parameter_list', text=parameter_list)

    # Visit a parse tree produced by GminorParser#procedure.
    def visitProcedure(self,
                       ctx: GminorParser.ProcedureContext,
                       is_syntax_pattern=False,
                       literals=tuple(),
                       pattern_variables=tuple()):

        procedure = '('
        procedure += Literal('lambda') if is_syntax_pattern else 'lambda '
        semantic_element = self.visitParameter_list(ctx.parameter_list(),
                                                    is_syntax_pattern,
                                                    literals,
                                                    pattern_variables)

        procedure += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)
        self._indent += INDENT
        for count, s_expr in enumerate(ctx.s_expression()):
            semantic_element = self.visitS_expression(s_expr, pattern_variables=pattern_variables)
            procedure_s_expression = unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)
            if count == 0 and not re.match(r'\s', procedure_s_expression):
                procedure += ' '

            procedure += procedure_s_expression

        procedure += ')'
        self._indent = self._indent[len(INDENT):]
        if is_syntax_pattern:
            return SemanticElement('procedure', parser_element=procedure)

        return SemanticElement('procedure', text=procedure)

    # Visit a parse tree produced by GminorParser#cond_expression.
    def visitCond_expression(self,
                             ctx: GminorParser.Cond_expressionContext,
                             is_syntax_pattern=False,
                             literals=tuple(),
                             pattern_variables=tuple()):

        cond_expr = '\n' + self._indent + '('
        cond_expr += Literal('cond') if is_syntax_pattern else 'cond'
        self._indent += INDENT
        for subexpression in ctx.cond_subexpression():
            cond_expr += '\n' + self._indent
            semantic_element = self.visitCond_subexpression(subexpression, is_syntax_pattern, literals, pattern_variables)
            cond_expr += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)

        if ctx.else_clause():
            cond_expr += '\n' + self._indent
            semantic_element = self.visitElse_clause(ctx.else_clause(), is_syntax_pattern, literals, pattern_variables)
            cond_expr += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)

        cond_expr += ')'
        self._indent = self._indent[len(INDENT):]

        if is_syntax_pattern:
            return SemanticElement('cond_expression', parser_element=cond_expr)

        return SemanticElement('cond_expression', text=cond_expr)

    # Visit a parse tree produced by GminorParser#cond_subexpression.
    def visitCond_subexpression(self,
                                ctx: GminorParser.Cond_subexpressionContext,
                                is_syntax_pattern=False,
                                literals=tuple(),
                                pattern_variables=tuple()):
        expression_list = '('
        s_expressions = ctx.s_expression()
        for count, s_expr in enumerate(s_expressions):
            if count > 0 and not is_syntax_pattern:
                expression_list += ' '

            semantic_element = self.visitS_expression(s_expr,
                                                      is_syntax_pattern=is_syntax_pattern,
                                                      literals=literals,
                                                      pattern_variables=pattern_variables)

            expression_list += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)

        expression_list += ')'
        if is_syntax_pattern:
            return SemanticElement('expression_list', parser_element=expression_list)

        return SemanticElement('expression_list', text=expression_list)

    # Visit a parse tree produced by GminorParser#else_clause.
    def visitElse_clause(self,
                         ctx: GminorParser.Else_clauseContext,
                         is_syntax_pattern=False,
                         literals=tuple(),
                         pattern_variables=tuple()):

        self._indent += INDENT
        else_clause = '('
        else_clause += Literal('else') if is_syntax_pattern else 'else'
        for count, s_expr in enumerate(ctx.s_expression()):
            else_clause += '\n' + self._indent
            if count > 0:
                else_clause += ' '

            semantic_element = self.visitS_expression(s_expr,
                                                      is_syntax_pattern=is_syntax_pattern,
                                                      literals=literals,
                                                      pattern_variables=pattern_variables)

            else_clause += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)

        else_clause += ')'
        self._indent = self._indent[len(INDENT):]
        if is_syntax_pattern:
            return SemanticElement('else_clause', parser_element=else_clause)

        return SemanticElement('else_clause', text=else_clause)

    # Visit a parse tree produced by GminorParser#let_binding.
    def visitLet_binding(self,
                         ctx: GminorParser.Let_bindingContext,
                         is_syntax_pattern=False,
                         literals=tuple(),
                         pattern_variables=tuple()):

        let_binding = '('
        semantic_element = self.visitIdentifier(ctx.identifier(), is_syntax_pattern, literals, pattern_variables)
        let_binding += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)
        let_binding += ' '
        semantic_element = self.visitS_expression(ctx.s_expression(),
                                                  is_syntax_pattern=is_syntax_pattern,
                                                  literals=literals,
                                                  pattern_variables=pattern_variables)

        let_binding += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)
        let_binding += ')'
        if is_syntax_pattern:
            return SemanticElement('let_binding', parser_element=let_binding)

        return SemanticElement('let_binding', text=let_binding)

    # Visit a parse tree produced by GminorParser#let_expression.
    def visitLet_expression(self,
                            ctx: GminorParser.Let_expressionContext,
                            is_syntax_pattern=False,
                            literals=tuple(),
                            pattern_variables=tuple()):

        let_expr = '('
        let_expr += Literal('let') if is_syntax_pattern else 'let'
        self._indent += INDENT
        let_expr += '\n' + self._indent + '('
        bindings = ctx.let_binding()
        for count, let_binding in enumerate(bindings):
            if count > 0:
                let_expr += '\n' + self._indent + ' '

            semantic_element = self.visitLet_binding(let_binding, is_syntax_pattern, literals, pattern_variables)
            let_expr += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)

        let_expr += ')'
        for count, s_expr in enumerate(ctx.s_expression()):
            if count > 0:
                let_expr += ' '

            if count < len(bindings) - 1:
                let_expr += '\n' + self._indent

            semantic_element = self.visitS_expression(s_expr,
                                                      is_syntax_pattern=is_syntax_pattern,
                                                      literals=literals,
                                                      pattern_variables=pattern_variables)

            let_expr += unbox_semantic_element(semantic_element, 2, is_syntax_pattern, 0)

        let_expr += ')'
        self._indent = self._indent[len(INDENT):]
        if is_syntax_pattern:
            return SemanticElement('let_expression', parser_element=let_expr)

        return SemanticElement('let_expression', text=let_expr)

    # Visit a parse tree produced by GminorParser#module_accessor.
    def visitModule_accessor(self, ctx: GminorParser.Module_accessorContext):
        module_text = ctx.identifier()[0].getText()
        module = self.symbol_table.find_symbol(self.mangled_names.get(module_text, module_text))
        ids = [self.visitIdentifier(i, suppress_substitution=True) for i in ctx.identifier()[1:]]
        try:
            # Treat the identifier as a reference to a macro in another module.
            namespace, _, mangled_names = module.value
            module_ids = [id_.text for id_ in ids]
            for module_id in module_ids[:-1]:
                mangled_name = mangled_names[module_id]
                symbol_descriptor = namespace.find_symbol(mangled_name)
                namespace, _, mangled_names = symbol_descriptor.value

            syntax_rules = namespace.find_symbol(mangled_names.get(module_ids[-1], module_ids[-1]))

            return SemanticElement('module_accessor', symbol_descriptor=syntax_rules)

        except AttributeError as attribute_error:
            # First id is not a symbol. This indicates that the module accessor
            # is part of a macro template.
            if str(attribute_error) != "'SemanticElement(identifier)' does not have 'symbol_descriptor'.":
                raise

            ids_string = '.'.join(id_.text for id_ in [module.id] + ids)
            return SemanticElement('module_accessor', text=ids_string)

        except symbols.NameNotFoundError:
            # First id is a symbol, but the subsequent id is not in the module's
            # symbol table. This indicates the reference is to a non-macro id.
            # Treat the identifier as a reference to a macro in another module.
            _, namespace, mangled_names = module.value
            module_accessor = module.id
            for module_id in module_ids[:-1]:
                mangled_name = mangled_names[module_id]
                module_accessor += '.' + mangled_name
                symbol_descriptor = namespace.find_symbol(mangled_name)
                _, namespace, mangled_names = symbol_descriptor.value

            mangled_name = mangled_names.get(module_ids[-1], module_ids[-1])
            symbol_descriptor = namespace.find_symbol(mangled_name)
            module_accessor += '.' + symbol_descriptor.id

            return SemanticElement('module_accessor', text=module_accessor)

    # Visit a parse tree produced by GminorParser#import_from.
    def visitImport_from(self, ctx: GminorParser.Import_fromContext):
        ids = [self.visitIdentifier(id_) for id_ in ctx.user_identifier()]
        module_name = ids[0].text
        module_vars = [id_.text for id_ in ids[1:]]
        variables_string = ', '.join("'{}'".format(v) for v in module_vars)
        self._logger.debug("Importing symbols %s from module '%s'", variables_string, module_name)
        module_path = find_module(module_name, self._working_directory)
        if not module_path:
            raise symbols.GminorImportError(self.symbol_table.get_stack_trace(), module_name)

        macro_symbols, regular_symbols, mangled_names = load_module(module_path)
        for module_var in module_vars:
            symbol_found = False
            symbol_name = mangled_names.get(module_var, module_var)
            mangled_name = mangle_name(module_name, symbol_name)
            self.mangled_names[module_var] = mangled_name
            try:
                # Search for ids in imported module's macro symbol table and
                # add them to this module's macro symbol table.
                macro_symbol = macro_symbols.find_symbol(symbol_name)
                macro_symbol.id = mangled_name
                self.symbol_table.add_symbol(macro_symbol)
                symbol_found = True

            except symbols.NameNotFoundError:
                # The referenced id is not a macro. This is fine; it may be a
                # non-macro symbol.
                pass

            try:
                # Try finding symbol in the non-macro symbol table.
                symbol = regular_symbols.find_symbol(symbol_name)
                symbol.id = mangled_name
                self.imported_symbols.append(symbol)

            except symbols.NameNotFoundError:
                if not symbol_found:
                    raise

        return SemanticElement('import_from', text='')

    # Visit a parse tree produced by GminorParser#module_import.
    def visitModule_import(self, ctx: GminorParser.Module_importContext):
        identifiers = [id_.getText() for id_ in ctx.user_identifier()]
        module_name = identifiers[0]
        self._logger.debug("Importing module '%s'", module_name)
        module_path = find_module(module_name, self._working_directory)
        if not module_path:
            raise symbols.GminorImportError(self.symbol_table.get_stack_trace(), module_name)

        macro_symbols, regular_symbols, mangled_names = load_module(module_path)
        original_name = module_name if len(identifiers) == 1 else identifiers[1]
        mangled_name = mangle_name(module_name)
        module_symbol_descriptor = symbols.SymbolDescriptor(mangled_name,
                                                            gminor_types.NamespaceType(),
                                                            (macro_symbols, regular_symbols, mangled_names))

        self.imported_symbols.append(module_symbol_descriptor)
        self.symbol_table.add_symbol(module_symbol_descriptor)
        self.mangled_names[original_name] = mangled_name

        return SemanticElement('module_import', text='')


class SemanticElement:
    """
    Describes a value returned by a leaf node in the parse tree.
    SemanticElements may only describe one type of value at a time.
    SemanticElement must be initialized with exactly one (and only one) of
    text, parser_element, or symbol_descriptor.

    Args
      caller: Name of the context in which SemanticElement is being generated.
      text: A string with the (possibly transformed) terminal text.
      parser_element: A pyparsing parser element (if the terminal is part of a syntax pattern).
      symbol_descriptor: An instance of symbols.SymbolDescriptor.

    """

    # Error message template used when a user of the instance attempts to access
    # an attribute corresponding to one of the optional constructor args that
    # was not given.
    ATTRIBUTE_ERROR_TEMPLATE = "'{semantic_element}' does not have '{attribute}'."

    def __init__(self, caller, text=None, parser_element=None, symbol_descriptor=None):
        if (text is not None) + (parser_element is not None) + (symbol_descriptor is not None) != 1:
            err = "SemanticElement not initialized with exactly one of 'text', 'parser_element', or 'syntax_rules'."
            raise ValueError(err)

        self.caller = caller
        self._text = text
        self._parser_element = parser_element
        self._symbol_descriptor = symbol_descriptor

    def __str__(self):
        return '{}({})'.format(type(self).__name__, self.caller)

    # Get the non-public version of an attribute value and raise an
    # AttributeError exception if it is None or does not exist.
    def __getattr__(self, name):
        real_name = '_' + name
        if real_name in self.__dict__:
            value = self.__dict__[real_name]
            if value is not None:
                return value

        err = self.ATTRIBUTE_ERROR_TEMPLATE.format(semantic_element=str(self),
                                                   attribute=name)

        raise AttributeError(err)
